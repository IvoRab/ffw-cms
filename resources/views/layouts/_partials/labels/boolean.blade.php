@if(!empty($label))
    <span class="label {{ !empty($labelType) ? 'label-' . $labelType : '' }} label-danger label-inline mr-2">
        {{ __('common.no') }}
    </span>
@else
    <span class="label {{ !empty($labelType) ? 'label-' . $labelType : '' }} label-success label-inline mr-2">
        {{ __('common.yes') }}
    </span>
@endif
