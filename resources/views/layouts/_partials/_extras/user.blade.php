<!--begin::Header-->
<div class="d-flex align-items-center p-8 rounded-top">
    <!--begin::Text-->
    <div class="text-dark m-0 flex-grow-1 mr-3 font-size-h5">{{ Auth::user()->name }}</div>
    <!--end::Text-->
</div>
<!--end::Header-->

<!--begin::Nav-->
<div class="navi navi-spacer-x-0 pt-5">

    <!--begin::Item-->
    <a href="{{--{{ route('UserProfile::index') }}--}}" class="navi-item px-8">
        <div class="navi-link">
            <div class="navi-icon mr-2">
                <i class="flaticon2-calendar-3 text-success"></i>
            </div>
            <div class="navi-text">
                <div class="font-weight-bold">{{ __('common.my_profile') }}</div>
                <div class="text-muted">{{ __('common.account_settings') }}</div>
            </div>
        </div>
    </a>
    <!--end::Item-->

    <!--begin::Footer-->
    <div class="navi-separator mt-3"></div>
    <div class="navi-footer px-8 py-5">
        <a href="javascript:;" data-url="{{ route('Auth::logout') }}" class="js-logout-btn btn btn-light-primary font-weight-bold">
            <i class="fa fa-key"></i> {{ __('Logout') }}
        </a>
    </div>
    <!--end::Footer-->

</div>
<!--end::Nav-->
