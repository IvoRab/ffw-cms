<?php

namespace App\Classes\Traits;

use App\Models\Page\PublishQueue;
use App\Models\Page\Keyword;
use Illuminate\Http\Request;
use App\Models\Page\Page;

trait PagesTrait
{
    /**
     * @param string $title
     * @param bool   $isHomePage
     *
     * @return string
     */
    public function createSEOUrl(string $title, bool $isHomePage = false): string
    {
        // the home page should not have any SEO URL
        if ($isHomePage) {
            return '';
        }

        $title = mb_strtolower(trim($title), 'utf-8');

        $url = preg_replace('/[^a-zA-Z0-9\s\'\:\/\[\]\-\p{L}]/u', '', $title);
        $url = preg_replace('/[\s\'\:\/\[\]\-]+/', ' ', $url);

        return str_replace([' ', '/'], '-', $url);
    }

    /**
     * @return array
     */
    public function getAllKeywords(): array
    {
        return Keyword::pluck('name')->toArray();
    }

    /**
     * @param \App\Models\Page\Page $page
     *
     * @return array
     */
    public function getPageKeywords(Page $page): array
    {
        $keywords = [];
        foreach ($page->keywords as $keyword) {
            $keywords[] = $keyword->name;
        }

        return $keywords;
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function getKeywordsIds(Request $request): array
    {
        $keywordIds = [];
        $keywords   = $request->get('keywords');
        if (empty($keywords)) {
            return $keywordIds;
        }

        // decode and convert to indexed array
        $keywords = array_column(json_decode($keywords, true), 'value');

        foreach ($keywords as $name) {
            // create the keyword if it doesn't exist
            $keyword      = Keyword::firstOrCreate(['name' => $name]);
            $keywordIds[] = $keyword->id;
        }

        return $keywordIds;
    }

    /**
     * Add a single page to the publish queue
     *
     * @param int $pageId
     */
    public function queueSinglePage(int $pageId)
    {
        PublishQueue::wherePageId($pageId)->firstOrCreate(['page_id' => $pageId]);
    }

    /**
     * Add all pages to the publish queue even if the page is not linked to a menu element
     */
    public function queueAllPages()
    {
        $pages = Page::get();

        foreach ($pages as $page) {
            $this->queueSinglePage($page->id);
        }
    }

    public function getStaticPageURL(string $seoUrl): string
    {

    }
}
