<!--begin: Search Form-->
<form class="kt-form kt-form--fit" data-table_id="{{ $dateTableId }}">
    <div class="row mb-5">
        <div class="col-md-2 mb-5">
            <label for="id">{!! __('common.id') !!}</label>
            <input type="text" class="form-control datatable-input" placeholder="{!! __('common.id') !!}"
                   data-col-index="0" name="id" id="id"/>
        </div>

        <div class="col-md-4 mb-5">
            <label for="name">{!! __('common.name') !!}</label>
            <input type="text" class="form-control datatable-input" placeholder="{!! __('common.name') !!}"
                   data-col-index="1" name="name" id="name"/>
        </div>

        <div class="col-md-4 mb-5">
            <label for="name">{!! __('common.name') !!}</label>
            <input type="text" class="form-control datatable-input" placeholder="{!! __('common.name') !!}"
                   data-col-index="2" name="name" id="name"/>
        </div>

        <div class="col-md-2 mb-5">
            <label for="is_published">{{ __('common.is_published') }}</label>
            <select class="form-control datatable-input" name="is_published" id="is_published" data-col-index="3">
                <option value="">{{ __('common.forms.select') }}</option>
                <option value="1">{{ trans('common.yes') }}</option>
                <option value="0">{{ trans('common.no') }}</option>
            </select>
        </div>

    </div>

    <div class="row">
        <div class="col-6">
            <button class="btn btn-primary btn-primary--icon kt-search mr-5" id="kt_search">
                <span>
                    <i class="flaticon-search"></i> <span>{{ __('common.buttons.search') }}</span>
                </span>
            </button>

            <button class="btn btn-secondary btn-secondary--icon kt-reset" id="kt_reset">
                <span>
                    <i class="flaticon2-delete"></i> <span>{{ __('common.buttons.reset') }}</span>
                </span>
            </button>
        </div>
    </div>
</form>

<div class="border-bottom w-100 mb-5 mt-10"></div>
<!--end: Search Form-->
