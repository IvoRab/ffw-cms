@extends('layouts.app')

@section('content')
    <!--begin::Card-->
    <div class="row">
        <div class="col-md-12">
            <div class="card card-custom">
                <div class="card-header flex-wrap pt-4 pb-4">
                    <div class="card-title">
                        <h3 class="card-label">
                            {{ $page->name ?? '' }}
                        </h3>
                    </div>

                    <div class="card-toolbar">
                        @include('layouts._partials.links.back', ['buttonRoute' => route('Pages::index')])
                        @include('layouts._partials.links.preview', ['buttonRoute' => route('Preview::index', ['slug' => $page->seo_url])])
                        @include('layouts._partials.links.edit', ['buttonRoute' => route('Pages::edit', ['id' => $page->id])])
                    </div>
                </div>
                <div class="card-body">
                    <div class="form-group row my-2">
                        <label class="col-2 col-form-label">Title:</label>
                        <div class="col-10">
                            <span class="form-control-plaintext font-weight-bolder">{{ $page->title }}</span>
                        </div>
                    </div>
                    <div class="form-group row my-2">
                        <label class="col-2 col-form-label">Body:</label>
                        <div class="col-10">
                            <span class="form-control-plaintext">{!! nl2br($page->body) !!}</span>
                        </div>
                    </div>
                    <div class="form-group row my-2">
                        <label class="col-2 col-form-label">Meta title:</label>
                        <div class="col-10">
                            <span class="form-control-plaintext font-weight-bolder">{{ $page->meta_title }}</span>
                        </div>
                    </div>
                    <div class="form-group row my-2">
                        <label class="col-2 col-form-label">Meta description:</label>
                        <div class="col-10">
                            <span class="form-control-plaintext font-weight-bolder">{{ $page->meta_description }}</span>
                        </div>
                    </div>
                    <div class="form-group row my-2">
                        <label class="col-2 col-form-label">Meta keywords:</label>
                        <div class="col-10">
                            @foreach($page->keywords as $keyword)
                            <span class="label label-inline">{{ $keyword->name }}</span>
                            @endforeach
                        </div>
                    </div>
                    <div class="form-group row my-2">
                        <label class="col-2 col-form-label">SEO URL:</label>
                        <div class="col-10">
                            <span class="form-control-plaintext font-weight-bolder">{{ $page->seo_url }}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end::Card-->
@endsection
