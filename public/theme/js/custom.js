var FFWCmsApp = {
    conf: {
        debug: true,
        page_name: "",
        is_page_load: 1
    },

    init: function () {
        var self = this;

        self.setPageName();
        self.logData('Page name', self.conf.page_name);

        if (self.conf.page_name === "pages") {
            if ($('#body').length) {
                self.logData('CMS page body element is found, initializing CKEditor');
                self.logData('Upload file url', $('form').data("upload-url"));
                ClassicEditor.create(
                    document.querySelector('#body'),
                    {ckfinder: {uploadUrl: $('form').data("upload-url")}}
                ).catch(error => {
                    console.error(error);
                });
            }
            if ($('#keywords').length) {
                self.keywordsTags();
            }
            if ($('#meta_title').length) {
                $(document).on('change', '#title', function () {
                    if (!$('#meta_title').val()) {
                        $('#meta_title').val($('#title').val());
                    }
                });
            }
        }

        if ($('.dd').length) {
            self.initNestable();
            self.initMenuModals();
        }

        if ($('.select2').length) {
            self.initSelect2();
        }

        if ($('#kt_logo, #kt_favicon').length) {
            self.initKTImageInput();
        }

        if ($('.js-datatable-ajax').length) {
            self.initDataTablesAjax()
        }

        if ($('.js-datatable-ajax-no-scroll').length) {
            self.initDataTablesAjaxNoScroll()
        }

        if ($('.js-datatable-scrollable').length) {
            self.initDataTablesScrollable()
        }

        if ($('.js-datatable-basic').length) {
            self.initDataTablesBasic()
        }
    },

    initNestable: function () {
        var self = this;
        self.logData('FFWCmsApp.initNestable called');

        $('.dd').nestable().on('change', self.nestableOutput);

        self.nestableOutput($('.dd').data('output', $('#nestable_output')));

        $('#nestable-menu').on('click', function (e) {
            var target = $(e.target);
            var action = target.data('action');

            console.log('Action: ', action);
            if (action === 'expand-all') {
                $('.dd').nestable('expandAll');
            }
            if (action === 'collapse-all') {
                $('.dd').nestable('collapseAll');
            }
        });
    },

    nestableOutput: function (e) {
        FFWCmsApp.logData('FFWCmsApp.nestableOutput called');

        var list = e.length ? e : $(e.target);
        var output = list.data('output');
        if (window.JSON) {
            var nestable_tree = window.JSON.stringify(list.nestable('serialize'));

            FFWCmsApp.logData('Menu tree', nestable_tree);

            // On page load do not execute the menu tree update request
            if (FFWCmsApp.conf.is_page_load === 0) {
                $.ajax({
                    url: $(".dd").data("url"),
                    type: "POST",
                    beforeSend: function (xhr, type) {
                        if (!type.crossDomain) {
                            xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
                            xhr.setRequestHeader('cache-control', "no-cache");
                        }
                    },
                    cache: false,
                    dataType: "json",
                    data: {
                        nestable_tree: nestable_tree
                    },
                    success: function (data) {
                        toastr.success(data.message);
                    }
                });
            }

            FFWCmsApp.conf.is_page_load = 0;
        }
    },

    initSelect2: function () {
        $('.select2').select2({
            placeholder: "Select a page",
            width: 'resolve'
        });
    },

    initMenuModals: function () {
        var self = this;

        $(document).on('click', '.create-menu-btn', function (e) {
            var $data = $(this).data();

            // set current menu id - represents the parent menu id for the new menu element
            $('#create-menu-modal input[name="parent_menu_id"]').val($data.parent_id);
        });

        $(document).on('click', '.edit-menu-btn', function (e) {
            var $data = $(this).data();

            // set current menu id - represents the parent menu id for the new menu element
            $('#edit-menu-modal input[name="id"]').val($data.id);
            $('#edit-menu-modal input[name="name"]').val($data.name);
            $('#edit-menu-modal select[name="page_id"]').val($data.page).trigger('change');
            $('#edit-menu-modal input[name="status"]').val($data.status).prop('checked', $data.status).trigger("change");

            // disabled page and status changes for the home menu element
            var propDisabled = $data.is_home_menu === 1;
            $('#edit-menu-modal select[name="page_id"]').prop("disabled", propDisabled);
            $('#edit-menu-modal input[name="status"]').prop("disabled", propDisabled);
        });

        $(document).on('click', '.delete-menu-btn', function (e) {
            var $data = $(this).data();

            // set current menu id - represents the parent menu id for the new menu element
            $('#delete-menu-modal input[name="id"]').val($data.id);
        });
    },

    initDataTablesAjax: function () {
        var self = this;

        self.logData('initDataTablesAjax started...');

        var tables = $('.js-datatable-ajax');

        // If tables present in page
        if (tables.length) {
            self.logData('initDataTablesAjax tables found');

            // Init each table
            $.each(tables, function () {
                var table_id = $(this).attr('id');

                var ajax_url = $(this).data('datatable_request_url');
                var form = $('[data-table_id="' + table_id + '"]');

                var is_large_data_table = $('.js-datatable-ajax.js-large-data').length;
                var table_length_menu = is_large_data_table ?
                    [[25, 50, 100, 250, 500, 1000, 5000, 7500, 10000, 15000, -1], [25, 50, 100, 250, 500, 1000, 5000, 7500, 10000, 15000, "All"]] :
                    [10, 25, 50, 100];
                var table_page_length = is_large_data_table ? 25 : 10;
                var table_buttons = is_large_data_table ? ['print', 'copyHtml5', 'excelHtml5', 'csvHtml5', 'pdfHtml5'] : [];
                var scroll_y = is_large_data_table ? '500px' : '250px';

                var table = $("#" + table_id).DataTable({
                    // responsive: true,
                    // dom: "<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>",

                    searchDelay: 2000,
                    minLength: 5,
                    serverSide: true,
                    stateSave: true,
                    autoWidth: true,
                    processing: true,
                    className: 'no-wrap',

                    scrollY: scroll_y,
                    scrollX: true,
                    scrollCollapse: true,
                    colReorder: false,
                    deferRender: true,

                    lengthMenu: table_length_menu,
                    pageLength: table_page_length,

                    stateSaveParams: function (oSettings, sValue) {
                        form.find(".form-control").each(function () {
                            sValue[$(this).attr('name')] = $(this).val();
                        });
                        return sValue;
                    },
                    stateLoadParams: function (oSettings, oData) {
                        //Load custom filters
                        form.find(".form-control").each(function () {
                            var element = $(this);
                            if (oData[element.attr('name')]) {
                                element.val(oData[element.attr('name')]);
                            }
                        });

                        return true;
                    },

                    ajax: {
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: ajax_url, // ajax source
                        type: "POST",
                        // timeout: 60000
                    },

                    buttons: table_buttons,

                    columnDefs: [
                        // {responsivePriority: 1, targets: 0, orderable: false},
                        {responsivePriority: 1, targets: 0, visible: true},
                        {responsivePriority: 2, targets: -1, visible: true},
                        // {responsivePriority: 3, targets: -1}
                    ]
                });

                table.on('responsive-resize', function (e, datatable, columns) {
                    var count = columns.reduce(function (a, b) {
                        return b === false ? a + 1 : a;
                    }, 0);

                    if (!count) $('#' + table_id).find('.my_control').addClass('hidden');
                    else $('#' + table_id).find('.my_control').removeClass('hidden');
                });

                if (form.length) {
                    form.on('click', '.kt-search', function (e) {
                        e.preventDefault();
                        var params = {};
                        form.find('.datatable-input').each(function () {
                            var i = $(this).data('col-index');
                            if (params[i]) {
                                params[i] += '|' + $(this).val();
                            } else {
                                params[i] = $(this).val();
                            }
                        });
                        $.each(params, function (i, val) {
                            // apply search params to datatable
                            table.column(i).search(val ? val : '', false, false);
                        });
                        table.table().draw();
                    });

                    form.on('click', '.kt-reset', function (e) {
                        e.preventDefault();
                        form.find('.datatable-input').each(function () {
                            if ($(this).attr('type') === 'date') {
                                Date.prototype.yyyymmdd = function () {
                                    var yyyy = this.getFullYear().toString();
                                    var mm = (this.getMonth() + 1).toString();
                                    var dd = this.getDate().toString();
                                    return yyyy + "-" + (mm[1] ? mm : "0" + mm[0]) + "-" + (dd[1] ? dd : "0" + dd[0]);
                                };

                                var date = new Date();
                                $(this).val(date.yyyymmdd());
                            } else {
                                $(this).val('');
                            }
                            table.column($(this).data('col-index')).search('', false, false);
                        });
                        table.table().draw();
                    });

                    $(document).keyup(function (event) {
                        if (event.keyCode === 13) {
                            $(".kt-search").click();
                        }
                        if (event.keyCode === 27) {
                            $(".kt-reset").click();
                        }
                    });

                    form.find('.kt-datepicker').datepicker({
                        todayHighlight: true,
                        autoclose: true,
                        templates: {
                            leftArrow: '<i class="la la-angle-left"></i>',
                            rightArrow: '<i class="la la-angle-right"></i>',
                        },
                    });
                }

                if (is_large_data_table) {
                    $('#export_print').on('click', function (e) {
                        e.preventDefault();
                        table.button(0).trigger();
                    });

                    $('#export_copy').on('click', function (e) {
                        e.preventDefault();
                        table.button(1).trigger();
                    });

                    $('#export_excel').on('click', function (e) {
                        e.preventDefault();
                        table.button(2).trigger();
                    });

                    $('#export_csv').on('click', function (e) {
                        e.preventDefault();
                        table.button(3).trigger();
                    });

                    $('#export_pdf').on('click', function (e) {
                        e.preventDefault();
                        table.button(4).trigger();
                    });
                }

                table.on('click', 'tbody tr', function () {
                    $(this).toggleClass('active');
                });

            });

        }

    },

    initDataTablesAjaxNoScroll: function () {
        self.logData('initDataTablesAjax started...');

        var tables = $('.js-datatable-ajax-no-scroll');

        // If tables present in page
        if (tables.length) {
            self.logData('initDataTablesAjax tables found');

            // Init each table
            $.each(tables, function () {
                var table_id = $(this).attr('id');
                var ajax_url = $(this).data('datatable_request_url');
                var form = $('[data-table_id="' + table_id + '"]');

                var is_large_data_table = $('.js-datatable-ajax.js-large-data').length;
                var table_length_menu = is_large_data_table ?
                    [[25, 50, 100, 250, 500, 1000, 5000, 7500, 10000, 15000, -1], [25, 50, 100, 250, 500, 1000, 5000, 7500, 10000, 15000, "All"]] :
                    [10, 25, 50, 100];
                var table_page_length = is_large_data_table ? 25 : 10;
                var table_buttons = is_large_data_table ? ['print', 'copyHtml5', 'excelHtml5', 'csvHtml5', 'pdfHtml5'] : [];
                var scroll_y = is_large_data_table ? '500px' : '250px';

                var table = $('#' + table_id).DataTable({
                    // responsive: true,
                    // dom: "<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>",
                    responsive: true,
                    searchDelay: 2000,
                    minLength: 5,
                    serverSide: true,
                    stateSave: true,
                    autoWidth: true,
                    processing: true,
                    className: 'no-wrap',

                    scrollY: false,
                    scrollX: false,
                    scrollCollapse: false,
                    colReorder: false,
                    deferRender: true,

                    lengthMenu: table_length_menu,
                    pageLength: table_page_length,

                    stateSaveParams: function (oSettings, sValue) {
                        form.find(".form-control").each(function () {
                            sValue[$(this).attr('name')] = $(this).val();
                        });
                        return sValue;
                    },
                    stateLoadParams: function (oSettings, oData) {
                        //Load custom filters
                        form.find(".form-control").each(function () {
                            var element = $(this);
                            if (oData[element.attr('name')]) {
                                element.val(oData[element.attr('name')]);
                            }
                        });

                        return true;
                    },

                    ajax: {
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: ajax_url, // ajax source
                        type: "POST",
                        // timeout: 60000
                    },

                    buttons: table_buttons,

                    columnDefs: [
                        // {responsivePriority: 1, targets: 0, orderable: false},
                        {responsivePriority: 1, targets: 0, visible: true},
                        {responsivePriority: 2, targets: -1, visible: true},
                        // {responsivePriority: 3, targets: -1}
                    ]
                });

                table.on('responsive-resize', function (e, datatable, columns) {
                    var count = columns.reduce(function (a, b) {
                        return b === false ? a + 1 : a;
                    }, 0);

                    if (!count) $('#' + table_id).find('.my_control').addClass('hidden');
                    else $('#' + table_id).find('.my_control').removeClass('hidden');
                });

                if (form.length) {
                    form.on('click', '.kt-search', function (e) {
                        e.preventDefault();
                        var params = {};
                        form.find('.datatable-input').each(function () {
                            var i = $(this).data('col-index');
                            if (params[i]) {
                                params[i] += '|' + $(this).val();
                            } else {
                                params[i] = $(this).val();
                            }
                        });
                        $.each(params, function (i, val) {
                            // apply search params to datatable
                            table.column(i).search(val ? val : '', false, false);
                        });
                        table.table().draw();
                    });

                    form.on('click', '.kt-reset', function (e) {
                        e.preventDefault();
                        form.find('.datatable-input').each(function () {
                            if ($(this).attr('type') === 'date') {
                                Date.prototype.yyyymmdd = function () {
                                    var yyyy = this.getFullYear().toString();
                                    var mm = (this.getMonth() + 1).toString();
                                    var dd = this.getDate().toString();
                                    return yyyy + "-" + (mm[1] ? mm : "0" + mm[0]) + "-" + (dd[1] ? dd : "0" + dd[0]);
                                };

                                var date = new Date();
                                $(this).val(date.yyyymmdd());
                            } else {
                                $(this).val('');
                            }
                            table.column($(this).data('col-index')).search('', false, false);
                        });
                        table.table().draw();
                    });

                    $(document).keyup(function (event) {
                        if (event.keyCode === 13) {
                            $(".kt-search").click();
                        }
                        if (event.keyCode === 27) {
                            $(".kt-reset").click();
                        }
                    });

                    form.find('.kt-datepicker').datepicker({
                        todayHighlight: true,
                        autoclose: true,
                        templates: {
                            leftArrow: '<i class="la la-angle-left"></i>',
                            rightArrow: '<i class="la la-angle-right"></i>',
                        },
                    });
                }

                if (is_large_data_table) {
                    $('#export_print').on('click', function (e) {
                        e.preventDefault();
                        table.button(0).trigger();
                    });

                    $('#export_copy').on('click', function (e) {
                        e.preventDefault();
                        table.button(1).trigger();
                    });

                    $('#export_excel').on('click', function (e) {
                        e.preventDefault();
                        table.button(2).trigger();
                    });

                    $('#export_csv').on('click', function (e) {
                        e.preventDefault();
                        table.button(3).trigger();
                    });

                    $('#export_pdf').on('click', function (e) {
                        e.preventDefault();
                        table.button(4).trigger();
                    });
                }

                table.on('click', 'tbody tr', function () {
                    $(this).toggleClass('active');
                });

            });
        }
    },

    initDataTablesScrollable: function () {
        self.logData('initDataTablesScrollable started...');

        var tables = $('.js-datatable-scrollable');

        if (tables.length) {
            self.logData('initDataTablesScrollable tables found');

            $.each(tables, function () {

                var table_id = $(this).attr('id');
                var table = $('#' + table_id).DataTable({
                    scrollY: '50vh',
                    scrollX: true,
                    scrollCollapse: true,
                    colReorder: true,
                    columnDefs: [
                        {
                            targets: -1,
                            orderable: false,
                            width: '50px',
                        }
                    ],
                    lengthMenu: [10, 25, 50, 75, 100],
                    pageLength: 10,
                    order: [
                        [0, 'asc']
                    ],
                });

                table.on('click', 'tbody tr', function () {
                    $(this).toggleClass('active');
                });
            });

        }
    },

    initDataTablesBasic: function () {
        self.logData('initDataTablesBasic started...');

        var tables = $('.js-datatable-basic');

        if (tables.length) {
            self.logData('initDataTablesBasic tables found');

            $.each(tables, function () {
                var is_compact_data_table = $('.js-datatable-basic.js-compact-data').length;
                var table_length_menu = is_compact_data_table ? [5, 10, 15, 25] : [10, 25, 50, 75, 100];
                var table_page_length = is_compact_data_table ? 5 : 25;

                var table_id = $(this).attr('id');
                var table = $('#' + table_id).DataTable({
                    responsive: true,
                    colReorder: true,
                    lengthMenu: table_length_menu,
                    pageLength: table_page_length,
                    // order: [
                    //     [0, 'desc']
                    // ],
                });

                table.on('click', 'tbody tr', function () {
                    $(this).toggleClass('active');
                });
            });

        }
    },

    keywordsTags: function () {
        var input = document.getElementById('keywords'),
            // init Tagify script on the above inputs
            tagify = new Tagify(input, {
                whitelist: keywords,
                blacklist: [], // <-- passed as an attribute in this demo
            })


        // "remove all tags" button event listener
        document.getElementById('keywords_remove').addEventListener('click', tagify.removeAllTags.bind(tagify))

        // Chainable event listeners
        tagify.on('add', onAddTag)
            .on('remove', onRemoveTag)
            .on('input', onInput)
            .on('edit', onTagEdit)
            .on('invalid', onInvalidTag)
            .on('click', onTagClick)
            .on('dropdown:show', onDropdownShow)
            .on('dropdown:hide', onDropdownHide)

        // tag added callback
        function onAddTag(e) {
            FFWCmsApp.logData("onAddTag: ", e.detail);
            FFWCmsApp.logData("original input value: ", input.value)
            tagify.off('add', onAddTag) // exmaple of removing a custom Tagify event
        }

        // tag remvoed callback
        function onRemoveTag(e) {
            FFWCmsApp.logData(e.detail);
            FFWCmsApp.logData("tagify instance value:", tagify.value)
        }

        // on character(s) added/removed (user is typing/deleting)
        function onInput(e) {
            FFWCmsApp.logData(e.detail);
            FFWCmsApp.logData("onInput: ", e.detail);
        }

        function onTagEdit(e) {
            FFWCmsApp.logData("onTagEdit: ", e.detail);
        }

        // invalid tag added callback
        function onInvalidTag(e) {
            FFWCmsApp.logData("onInvalidTag: ", e.detail);
        }

        // invalid tag added callback
        function onTagClick(e) {
            FFWCmsApp.logData(e.detail);
            FFWCmsApp.logData("onTagClick: ", e.detail);
        }

        function onDropdownShow(e) {
            FFWCmsApp.logData("onDropdownShow: ", e.detail)
        }

        function onDropdownHide(e) {
            FFWCmsApp.logData("onDropdownHide: ", e.detail)
        }
    },

    initKTImageInput: function () {
        var logo = new KTImageInput('kt_logo');
        var kt_favicon = new KTImageInput('kt_favicon');
    },

    setPageName: function () {
        var self = this;

        self.conf.page_name = $('body').data("page-name");
    },

    logData: function (text, variable) {
        var self = this;
        var value = variable || null;

        if (self.conf.debug && window.console) {
            if (value) {
                console.log(text, variable);
            } else {
                console.log(text);
            }
        }
    }
};

/*
var KTLayoutAsideToggle = function () {
    // Private properties
    var _body;
    var _element;
    var _toggleObject;

    // Initialize
    var _init = function () {
        _toggleObject = new KTToggle(_element, {
            target: _body,
            targetState: 'aside-minimize',
            toggleState: 'active'
        });

        _toggleObject.on('toggle', function (toggle) {
            // Update sticky card
            if (typeof KTLayoutStickyCard !== 'undefined') {
                KTLayoutStickyCard.update();
            }

            // Pause header menu dropdowns
            if (typeof KTLayoutHeaderMenu !== 'undefined') {
                KTLayoutHeaderMenu.pauseDropdownHover(800);
            }

            // Pause aside menu dropdowns
            if (typeof KTLayoutAsideMenu !== 'undefined') {
                KTLayoutAsideMenu.pauseDropdownHover(800);
            }

            // Remember state in cookie
            KTCookie.setCookie('kt_aside_toggle_state', toggle.getState());
            // to set default minimized left aside use this cookie value in your
            // server side code and add "kt-primary--minimize aside-minimize" classes to
            // the body tag in order to initialize the minimized left aside mode during page loading.
        });

        _toggleObject.on('beforeToggle', function (toggle) {
            $('.dataTables_wrapper').each(function () {
                $(this).find('table').DataTable().draw();
            });
            if (KTUtil.hasClass(_body, 'aside-minimize') === false && KTUtil.hasClass(_body, 'aside-minimize-hover')) {
                KTUtil.removeClass(_body, 'aside-minimize-hover');
            }
        });
    }

    // Public methods
    return {
        init: function (id) {
            _element = KTUtil.getById(id);
            _body = KTUtil.getBody();

            if (!_element) {
                return;
            }

            // Initialize
            _init();
        },

        getElement: function () {
            return _element;
        },

        getToggle: function () {
            return _toggleObject;
        },

        onToggle: function (handler) {
            if (typeof _toggleObject.element !== 'undefined') {
                _toggleObject.on('toggle', handler);
            }
        }
    };
}();
*/

$(document).ready(function () {
    console.log('FFWCmsApp JS Loaded');

    FFWCmsApp.init();

    // KTLayoutAsideToggle.init('kt_aside_toggle');
});
