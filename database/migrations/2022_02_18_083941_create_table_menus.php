<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableMenus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('menus');

        Schema::create('menus', function (Blueprint $table) {
            $table->id();
            $table->tinyInteger('is_home_menu')->unsigned()->default(0);
            $table->unsignedBigInteger('parent_menu_id');
            $table->bigInteger('page_id')->unsigned()->default(0);
            $table->string('name');
            $table->integer('position')->unsigned();
            $table->tinyInteger('status')->unsigned()->default(1);
            $table->timestamps();
        });

        // Get home page
        $page = \Illuminate\Support\Facades\DB::table('pages')->where('is_home_page', 1)->first();

        // Create home menu element
        \Illuminate\Support\Facades\DB::table('menus')->insert(
            [
                'is_home_menu'   => 1,
                'parent_menu_id' => 0,
                'page_id'        => !empty($page) ? $page->id : 0,
                'name'           => 'Home',
                'position'       => 1,
                'status'         => 1,
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('menus');
    }
}
