@foreach($menus as $menu)
    @if(!empty($menu['children']))
        <li class="dd-item dd3-item" data-id="{{ $menu['id'] }}" data-children="{{ count($menu['children']) }}">
            <div class="dd-handle dd3-handle"></div>
            <div class="dd3-content {{ !$menu['status'] ? 'inactive' : '' }}">
                {{ $menu['name'] }}
                <span class="menu-page">
                    @if(!empty($menu['page']))
                        [{{ $menu['page'] }}]
                    @endif
                </span>
                <span class="action-buttons">
                    @include('menu._partials.modal_create_button', ['id' => $menu['id']])
                    @include('menu._partials.modal_edit_button', ['menu' => $menu])
                    @include('menu._partials.modal_delete_button', ['id' => $menu['id']])
                </span>
            </div>
            <ol class="dd-list">
                @include('menu._partials.element', ['menus' => $menu['children']])
            </ol>
        </li>
    @else
        <li class="dd-item dd3-item" data-id="{{ $menu['id'] }}" data-children="{{ count($menu['children']) }}">
            <div class="dd-handle dd3-handle"></div>
            <div class="dd3-content {{ !$menu['status'] ? 'inactive' : '' }}">
                {{ $menu['name'] }}
                <span class="menu-page">
                    @if(!empty($menu['page']))
                        [{{ $menu['page'] }}]
                    @endif
                </span>
                <span class="action-buttons">
                    @include('menu._partials.modal_create_button', ['id' => $menu['id']])
                    @include('menu._partials.modal_edit_button', ['menu' => $menu])
                    @include('menu._partials.modal_delete_button', ['id' => $menu['id']])
                </span>
            </div>
        </li>
    @endif
@endforeach

{{--dd-nodrag--}}
