<?php

namespace App\Observers;

use App\Classes\Traits\PagesTrait;
use App\Models\Menu\Menu;
use App\Models\Page\Page;

class PageObserver
{
    use PagesTrait;

    /**
     * Handle events after all transactions are committed.
     *
     * @var bool
     */
    public $afterCommit = false;

    /**
     * Handle the Page\Page "updated" event.
     *
     * @param \App\Models\Page\Page $page
     *
     * @return void
     */
    public function saving(Page $page)
    {
        $originalPage = Page::whereId($page->id)->first();

        // Method is called before the transaction is committed
        // when creating new page, the objects Page and originalPage does not exist yet in DB
        if (!empty($originalPage)) {
            $seoUrlIsModified = (int)($originalPage->seo_url != $page->seo_url);

            switch ($seoUrlIsModified) {
                case 1:
                    // publish all the pages if the SEO URL is modified to avoid broken links in the navigation
                    $this->queueAllPages();
                    break;
                default:
                    // publish only the modified page content
                    $this->queueSinglePage($page->id);
                    break;
            }
        }
    }

    /**
     * Handle the Page "deleted" event.
     *
     * @param \App\Models\Page\Page $page
     *
     * @return void
     */
    public function deleted(Page $page)
    {
        $menus = Menu::wherePageId($page->id)->get();

        // If the page is linked to a menu element publish all pages to avoid broken links in the navigation
        if (!empty($menus)) {
            $this->queueAllPages();
        }
    }
}
