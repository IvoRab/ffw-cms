@extends('layouts.app')

@section('content')
    <!--begin::Card-->
    <div class="card card-custom">
        <div class="card-header flex-wrap pt-4 pb-4">
            <div class="card-title">
                <h3 class="card-label">
                    {{ !empty($pageTitle) ? $pageTitle : '' }}
                </h3>
            </div>

            <div class="card-toolbar">
                @include('layouts._partials.links.create', ['buttonRoute' => route('Pages::create')])
            </div>
        </div>

        <div class="card-body">
            <!--begin: Search Form-->
        @include('pages._partials.filters')
        <!--end: Search Form-->

            <!--begin: Datatable-->
            <div class="row">
                <div class="col-12">
                    <table
                        class="table table-bordered table-hover table-striped table-checkable js-datatable-ajax js-large-data"
                        id="{{ $dateTableId }}" data-datatable_request_url="{{ route('Pages::gridData') }}"
                        style="margin-top: 13px !important">
                        <thead>
                        <tr>
                            <th>{!! __('common.id') !!}</th>
                            <th>{!! __('common.name') !!}</th>
                            <th>{!! __('common.is_published') !!}</th>
                            <th>{!! __('common.created_at') !!}</th>
                            <th>{!! __('common.updated_at') !!}</th>
                            <th>{{ __('common.actions') }}</th>
                        </tr>
                        </thead>

                        <tbody></tbody>
                    </table>
                </div>
            </div>
            <!--end: Datatable-->
        </div>
    </div>
    <!--end::Card-->
@endsection

@section('page-plugins-styles')
    <link href="{{ asset('theme/assets/plugins/custom/datatables/datatables.bundle.css') }}"
          rel="stylesheet" type="text/css"/>
@endsection

@section('page-plugins-scripts')
    <script src="{{ asset('theme/assets/plugins/custom/datatables/datatables.bundle.js') }}"
            type="text/javascript"></script>
@endsection
