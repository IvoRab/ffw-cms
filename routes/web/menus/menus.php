<?php

Route::group(
    [
        'namespace' => 'Menu',
        'as'        => 'Menus::',
    ],
    function () {
        Route::get('/', [\App\Http\Controllers\Menu\MenusController::class, 'index'])->name('index');
        Route::post('store', [\App\Http\Controllers\Menu\MenusController::class, 'store'])->name('store');
        Route::post('update', [\App\Http\Controllers\Menu\MenusController::class, 'update'])->name('update');
        Route::post('delete', [\App\Http\Controllers\Menu\MenusController::class, 'delete'])->name('delete');
        Route::post('tree/update', [\App\Http\Controllers\Menu\MenusController::class, 'updateTree'])->name('updateTree');
    }
);
