<?php

namespace App\Classes\Interfaces\Publisher;

interface PublisherInterface
{
    public function createAssets(): void;

    public function createPage($menus, $page, $keywords, $mainUrl): void;

    public function publish(): void;

    public function upload(): void;

    public function archive(): void;
}
