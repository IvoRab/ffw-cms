<?php

Route::group(
    [
        'namespace' => 'Publish',
        'as'        => 'Publish::',
    ],
    function () {
        Route::get('/', [\App\Http\Controllers\Publish\PublishController::class, 'index'])->name('index');
        Route::post('store', [\App\Http\Controllers\Publish\PublishController::class, 'store'])->name('store');
        Route::get('download', [\App\Http\Controllers\Publish\PublishController::class, 'download'])->name('download');
    }
);
