@extends('auth.layout')

@section('content')
    <form class="form" id="" method="POST" data-action="{{ route('Auth::login') }}">
        {{ csrf_field() }}

        <div class="fv-plugins-message-container">
            <div data-field="password" data-validator="notEmpty" class="fv-help-block d-none" id="login_error_message">
                {{ __('auth.failed') }}
            </div>
        </div>

        <div class="form-group mb-5">
            <input class="form-control h-auto form-control-solid py-4 px-8" type="text"
                   placeholder="{{ __('E-Mail Address') }}" name="email" id="email" autofocus
                   value="{{ old('email') }}"/>
        </div>

        <div class=" form-group mb-5">
            <input class="form-control h-auto form-control-solid py-4 px-8" type="password"
                   placeholder="{{ __('Password') }}" name="password" id="password"/>
        </div>

        <button type="submit" id="kt_submit_login"
                class="btn btn-primary font-weight-bold px-9 py-4 my-3 mx-4">{{ __('Login') }}
        </button>
    </form>
@endsection
