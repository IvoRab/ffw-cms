<a href="{{ $buttonRoute }}" class="btn btn-info mr-3">
    <i class="fas fa-indent mr-1"></i> {{ __('common.buttons.summary') }}
</a>