<a href="{{ !empty($buttonRoute) ? $buttonRoute : 'javascript:;' }}" {{ !empty($dataId) ? 'data-id=' . $dataId : '' }}
   class="btn btn-danger mr-3 {{ !empty($buttonType) ? 'btn-' . $buttonType : '' }} {{ !empty($class) ? $class : '' }}">
    <i class="flaticon2-rubbish-bin-delete-button mr-1"></i> {{ !empty($buttonNoLabel) ? '' : __('common.buttons.delete') }}
</a>