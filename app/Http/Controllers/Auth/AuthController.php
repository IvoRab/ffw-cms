<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\UserLoginRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthController extends Controller
{
    /**
     * AuthController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['index', 'login', 'register', 'refresh']]);

        $this->setPageName('login');
        $this->setPageTitle(__('common.login'));

        parent::__construct();
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(Request $request)
    {
        // If the user is not logged in but the JWT token is sent in the headers - load the refresh token page
        if (!auth()->check() && $request->header('Authorization')) {
            $this->setPageName('refresh-token');
            $this->setPageTitle(__('common.refresh_token'));

            return view('auth.refresh');
        }

        return view('auth.login');
    }

    /**
     * @param \App\Http\Requests\User\UserLoginRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(UserLoginRequest $request): \Illuminate\Http\JsonResponse
    {
        $this->getGuard()->factory()->setTTL(env('JWT_TTL', 60));
        $token = $this->getGuard()->attempt($request->only(['email', 'password']));
        if (empty($token)) {
            return response()->json(['message' => __('auth.failed'),], 401);
        }

        return response()->json(
            [
                'user'           => $this->getGuard()->user(),
                'token'          => $token,
                'token_lifetime' => $this->getGuard()->factory()->getTTL(),
                'redirect_url'   => route('Dashboard::index'),
            ]
        )->withCookie(cookie(env('JWT_COOKIE_NAME'), $token, env('COOKIE_TTL')));
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(): \Illuminate\Http\JsonResponse
    {
        $this->getGuard()->logout();

        return response()->json(
            [
                'message'      => __('auth.success_logout'),
                'redirect_url' => route('login'),
            ]
        )->withoutCookie(env('JWT_COOKIE_NAME'));
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh(): \Illuminate\Http\JsonResponse
    {
        $token = $this->getGuard()->refresh();

        if (empty($token)) {
            return response()->json(
                [
                    'message'      => __('auth.refresh_token_failed'),
                    'redirect_url' => route('login'),
                ]
            )->withoutCookie(env('JWT_COOKIE_NAME'));
        }

        return response()->json(
            [
                'token'          => $token,
                'token_lifetime' => $this->getGuard()->factory()->getTTL(),
                'redirect_url'   => route('Dashboard::index'),
            ]
        )->withCookie(cookie(env('JWT_COOKIE_NAME'), $token, env('COOKIE_TTL')));
    }
}
