<div class="d-flex justify-content-center">
    <button type="submit" class="btn btn-success col-4 btn-block"
            name="confirmOrder">
        {{ __('pages/orders.buttons.confirm_orders') }}
    </button>
</div>