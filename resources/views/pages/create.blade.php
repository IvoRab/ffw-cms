@extends('layouts.app')

@section('content')
    <!--begin::Card-->
    <div class="row">
        <div class="col-md-12">
            <div class="card card-custom">
                <div class="card-header flex-wrap pt-4 pb-4">
                    <div class="card-title">
                        <h3 class="card-label">
                            {{ !empty($pageTitle) ? $pageTitle : '' }}
                        </h3>
                    </div>

                    <div class="card-toolbar">
                        @include('layouts._partials.links.back', ['buttonRoute' => route('Pages::index')])
                    </div>
                </div>

                <!--begin: Form-->
                <form action="{{ route('Pages::store') }}" method="post"
                      class="form-horizontal" enctype="multipart/form-data"
                      data-upload-url="{{ route('Pages::upload') . '?_token=' . csrf_token() }}">

                    <div class="card-body">
                        @include('pages._partials.form_body')
                    </div>

                    <div class="card-footer text-center">
                        {{ csrf_field() }}
                        @include('layouts._partials.buttons.save')
                        @include('layouts._partials.links.cancel', ['buttonRoute' => route('Pages::index')])
                    </div>

                </form>
                <!--end: Form-->

            </div>
        </div>
    </div>
    <!--end::Card-->
@endsection

@section('page-plugins-scripts')
    <script src="{{ asset('theme/js/ckeditor.js') }}" type="text/javascript"></script>
@endsection

@section('page-scripts')
    <script type="text/javascript">
        var keywords = @json($allKeywords);
    </script>
@endsection
