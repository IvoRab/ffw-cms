<?php

namespace App\Http\Controllers\Page;

use App\Http\Requests\Page\PageCreateOrUpdateRequest;
use App\Classes\Interfaces\Page\PageFactory;
use App\Http\Controllers\Controller;
use App\Classes\Traits\PagesTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\Page\Page;
use Carbon\Carbon;

class PagesController extends Controller
{
    use PagesTrait;

    /**
     * PageController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth:api');

        $this->setPageTitle(__('common.sidebar.pages'));
        $this->setPageName('pages');
        $this->setDataTableId('reason_codes');

        parent::__construct();
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        return view('pages.index');
    }


    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function gridData(Request $request)
    {
        $iTotalRecords  = Page::count();
        $iDisplayLength = (int)($request->get('length'));
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart  = (int)($request->get('start'));
        $sEcho          = (int)($request->get('draw'));

        $dataFilterEloquent = Page::select(
            [
                'id',
                'name',
                'created_at',
                'updated_at',
                DB::raw('IF((SELECT id FROM page_publish_queues WHERE page_publish_queues.page_id = pages.id) > 0, 0, 1) as is_published'),
            ]
        );

        // Filters
        $columns = $request->get('columns');

        if (!empty($id = $columns[0]['search']['value'])) {
            $dataFilterEloquent->where('pages.id', $id);
        }

        if (!empty($name = $columns[1]['search']['value'])) {
            $dataFilterEloquent->where('name', 'LIKE', '%' . $name . '%');
        }

        if (!empty($title = $columns[2]['search']['value'])) {
            $dataFilterEloquent->where('title', 'LIKE', '%' . $title . '%');
        }

        if (isset($columns[7]['search']['value'])) {
            if ($columns[7]['search']['value']) {
                $dataFilterEloquent->where('is_published', 1);
            } else {
                $dataFilterEloquent->where('is_published', 0);
            }
        }

        // Search
        $search      = $request->get('search');
        $searchValue = $search['value'] ?? '';
        if (!empty($searchValue)) {
            $dataFilterEloquent->where(function ($query) use ($searchValue) {
                $query->where('id', $searchValue)
                      ->orWhere('name', 'LIKE', '%' . $searchValue . '%')
                      ->orWhere('title', 'LIKE', '%' . $searchValue . '%');
            });
        }

        // Sorting
        $order = $request->get('order');
        if (isset($order[0]['column'])) {
            $orderDirection = mb_strtoupper($order[0]['dir']);
            switch ($order[0]['column']) {
                case 1:
                    $dataFilterEloquent->orderBy('name', $orderDirection);
                    break;
                case 2:
                    $dataFilterEloquent->orderBy('is_published', $orderDirection);
                    break;
                case 3:
                    $dataFilterEloquent->orderBy('created_at', $orderDirection);
                    break;
                case 4:
                    $dataFilterEloquent->orderBy('updated_at', $orderDirection);
                    break;
                default:
                    $dataFilterEloquent->orderBy('id', $orderDirection);
                    break;
            }
        }

        $iFilteredRecords = $dataFilterEloquent->count();
        $data             = $dataFilterEloquent->skip($iDisplayStart)->limit($iDisplayLength)->get();

        $records = [
            'data' => [],
        ];

        foreach ($data as $row) {
            $records['data'][] = [
                $row->id,
                $row->name,
                view(
                    'layouts._partials.labels.boolean',
                    ['labelStatus' => $row->is_published]
                )->render(),
                Carbon::parse($row->created_at)->format('Y-m-d H:i:s'),
                Carbon::parse($row->updated_at)->format('Y-m-d H:i:s'),
                view(
                    'layouts._partials.links.view',
                    ['buttonRoute' => route('Pages::view', ['id' => $row->id]), 'buttonType' => 'xs']
                )->render() .
                view(
                    'layouts._partials.links.edit',
                    ['buttonRoute' => route('Pages::edit', ['id' => $row->id]), 'buttonType' => 'xs']
                )->render(),
            ];
        }

        $records['draw']            = $sEcho;
        $records['recordsTotal']    = $iTotalRecords;
        $records['recordsFiltered'] = $iFilteredRecords;

        return response()->json($records);
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {
        $this->setPageTitle('Create page');

        return view('pages.create', ['allKeywords' => $this->getAllKeywords()]);
    }

    /**
     * @param \App\Http\Requests\Page\PageCreateOrUpdateRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function store(PageCreateOrUpdateRequest $request)
    {
        $cmsPage = new PageFactory();
        $page    = $cmsPage->store($request);

        return redirect()
            ->route('Pages::view', ['id' => $page->id])
            ->with(
                [
                    'messages' => [
                        'success' =>
                            'Page "' . $page->name . '" created Successfully!',
                    ],
                ]
            );
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit($id)
    {
        $cmsPage = new PageFactory();
        $page    = $cmsPage->get($id);
        $this->setPageTitle('Edit page');

        return view(
            'pages.edit',
            [
                'page'        => $page,
                'pageKeyword' => $this->getPageKeywords($page),
                'allKeywords' => $this->getAllKeywords(),
            ],
        );
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function view($id)
    {
        $cmsPage = new PageFactory();

        return view('pages.view', ['page' => $cmsPage->get($id)]);
    }

    /**
     * @param \App\Http\Requests\Page\PageCreateOrUpdateRequest $request
     * @param                                                   $id
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function update(PageCreateOrUpdateRequest $request, $id)
    {
        $cmsPage = new PageFactory();
        $page    = $cmsPage->update($request, $id);

        return redirect()
            ->route('Pages::view', ['id' => $page->id])
            ->with(
                [
                    'messages' => [
                        'success' =>
                            'Page "' . $page->name . '" updated Successfully!',
                    ],
                ]
            );
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id)
    {
        $cmsPage = new PageFactory();
        $cmsPage->delete($id);

        return redirect()
            ->route('Pages::index')
            ->with(
                [
                    'messages' => [
                        'success' =>
                            'Page deleted Successfully!',
                    ],
                ]
            );
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function upload(Request $request)
    {
        if (!$request->hasFile('upload')) {
            return response()->json(['uploaded' => 0]);
        }

        $originName = $request->file('upload')->getClientOriginalName();
        $fileName   = pathinfo($originName, PATHINFO_FILENAME);
        $extension  = $request->file('upload')->getClientOriginalExtension();
        $fileName   = Str::slug($fileName) . '_' . time() . '.' . $extension;

        $request->file('upload')->move(public_path(Page::MEDIA_PATH), $fileName);

        return response()
            ->json(
                [
                    'fileName' => $fileName,
                    'uploaded' => 1,
                    'url'      => asset(Page::MEDIA_PATH . '/' . $fileName
                    ),
                ]
            );
    }
}
