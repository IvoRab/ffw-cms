<?php

namespace App\Http\Controllers\Menu;

use App\Classes\Interfaces\Menu\MenuTreeFactory;
use App\Classes\Interfaces\Menu\MenuFactory;
use App\Http\Controllers\Controller;
use App\Classes\Traits\MenusTrait;
use Illuminate\Http\Request;
use App\Models\Page\Page;

class MenusController extends Controller
{
    use MenusTrait;

    /**
     * MenusController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth:api');

        $this->setPageTitle(__('common.sidebar.menus'));
        $this->setPageName('pages');
        $this->setDataTableId('reason_codes');

        parent::__construct();
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $menuTreeFactory = new MenuTreeFactory();
        $menus           = $menuTreeFactory->create($this->getMainMenuIds());

        return view('menu.index', ['menus' => $menus, 'pages' => Page::get()]);
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function store(Request $request)
    {
        $menuFactory = new MenuFactory();
        $menu        = $menuFactory->store($request);

        return redirect()
            ->route('Menus::index')
            ->with(
                [
                    'messages' => [
                        'success' =>
                            'Menu "' . $menu->name . '" created Successfully!',
                    ],
                ]
            );
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function update(Request $request)
    {
        $menuFactory = new MenuFactory();
        $menu        = $menuFactory->update($request, $request->get('id'));

        return redirect()
            ->route('Menus::index')
            ->with(
                [
                    'messages' => [
                        'success' =>
                            'Menu "' . $menu->name . '" updated Successfully!',
                    ],
                ]
            );
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(Request $request)
    {
        $menuFactory = new MenuFactory();
        $menuFactory->delete($request->get('id'));

        return redirect()
            ->route('Menus::index')
            ->with(
                [
                    'messages' => [
                        'success' =>
                            'Menu deleted Successfully!',
                    ],
                ]
            );
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateTree(Request $request)
    {
        $menus           = !empty($request->get('nestable_tree')) ? json_decode($request->get('nestable_tree')) : [];
        $menuTreeFactory = new MenuTreeFactory();
        $menuTreeFactory->update($menus);

        return response()->json(['message' => "Menu updated successfully"]);
    }
}
