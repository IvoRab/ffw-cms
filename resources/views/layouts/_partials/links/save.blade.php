<a href="{{ $buttonRoute }}"
   class="btn btn-success mr-3 {{ !empty($buttonType) ? 'btn-' . $buttonType : '' }}">
    <i class="flaticon2-check-mark mr-1"></i> {{ !empty($buttonNoLabel) ? '' : __('common.buttons.save') }}
</a>