<?php

namespace App\Classes\Interfaces\Page;

use App\Classes\Traits\PagesTrait;
use Illuminate\Http\Request;
use App\Models\Page\Page;
use Exception;

class PageFactory implements PageFactoryInterface
{
    use PagesTrait;

    /**
     * @param int $id
     *
     * @return \App\Models\Page\Page|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
     */
    public function get(int $id)
    {
        return Page::whereId($id)->firstOrFail();
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \App\Models\Page\Page
     * @throws \Exception
     */
    public function store(Request $request): \App\Models\Page\Page
    {
        try {
            $page = Page::create(
                [
                    'name'             => $request->get('name'),
                    'title'            => $request->get('title'),
                    'body'             => $request->get('body'),
                    'meta_title'       => $request->get('meta_title'),
                    'meta_description' => $request->get('meta_description'),
                    'seo_url'          => !empty($request->get('seo_url')) ?
                        $this->createSEOUrl($request->get('seo_url')) : $this->createSEOUrl($request->get('name')),
                ]
            );

            if (!empty($request->get('keywords'))) {
                $page->keywords()->attach($this->getKeywordsIds($request));
            }

            // Add the page to publish queue in case it's a back/hidden link
            $this->queueSinglePage($page->id);

            return $page;
        } catch (Exception $exception) {
            throw new Exception($exception->getMessage() . ' [' . $exception->getTraceAsString() . ']');
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \App\Models\Page\Page|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
     * @throws \Exception
     */
    public function update(Request $request, int $id)
    {
        try {
            $page   = Page::whereId($id)->firstOrFail();
            $seoUrl = !empty($request->get('seo_url')) ?
                $this->createSEOUrl($request->get('seo_url'), (bool)$page->is_home_page) :
                $this->createSEOUrl($request->get('name'), (bool)$page->is_home_page);

            $page->update(
                [
                    'name'             => $request->get('name'),
                    'title'            => $request->get('title'),
                    'body'             => $request->get('body'),
                    'meta_title'       => $request->get('meta_title'),
                    'meta_description' => $request->get('meta_description'),
                    'seo_url'          => $seoUrl,
                ]
            );

            $page->keywords()->detach();
            if (!empty($request->get('keywords'))) {
                $page->keywords()->attach($this->getKeywordsIds($request));
            }

            return $page;
        } catch (Exception $exception) {
            throw new Exception($exception->getMessage());
        }
    }

    /**
     * @param int $id
     */
    public function delete(int $id): void
    {
        $page = Page::whereId($id)->firstOrFail();
        $this->deletePublishQueue($page);
        $page->delete();
    }

    /**
     * @param \App\Models\Page\Page $page
     */
    public function deletePublishQueue(Page $page): void
    {
        if (!empty($page->publishQueue()->id)) {
            $page->publishQueue()->delete();
        }
    }
}
