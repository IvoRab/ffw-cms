@if (session('messages'))
    @foreach (session('messages') as $type => $message)
        @if ($type == 'danger')
            @if(is_array($message))
                @foreach ($message AS $singleMessage)
                    <script>
                        toastr.options.closeButton = true;
                        toastr.error({{ $single_message }});
                    </script>
                @endforeach
            @else
                <script>
                    toastr.options.closeButton = true;
                    toastr.error("{{ $message }}");
                </script>
            @endif
        @else
            @if(is_array($message))
                @foreach ($message AS $singleMessage)
                    <script>
                        toastr.options.closeButton = true;
                        toastr.success("{{ $single_message }}");
                    </script>
                @endforeach
            @else
                <script>
                    toastr.options.closeButton = true;
                    toastr.success("{{ $message }}");
                </script>
            @endif
        @endif
    @endforeach
@endif
