@if(!empty($labelStatus))
    <span class="label {{ !empty($labelType) ? 'label-' . $labelType : '' }} label-danger label-inline mr-2">
        {{ __('common.inactive') }}
    </span>
@else
    <span class="label {{ !empty($labelType) ? 'label-' . $labelType : '' }} label-success label-inline mr-2">
        {{ __('common.active') }}
    </span>
@endif