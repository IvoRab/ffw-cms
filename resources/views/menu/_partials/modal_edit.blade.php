<!-- Modal -->
<div class="modal fade" id="edit-menu-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <form action="{{ route('Menus::update') }}"
                  method="post" class="form-horizontal"
                  enctype="multipart/form-data">
                <div class="modal-header">
                    <h4 class="modal-title">Update menu</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="form-group">
                        <label class="col-12 control-label" for="name">
                            Name <span class="text-danger">*</span>
                        </label>
                        <div class="col-lg-8 col-md-8 col-sm-12">
                            <input type="text" class="form-control" placeholder="Menu name"
                                   name="name" id="name" value="">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-12 control-label" for="kt_select2_edit_menu">
                            Page <span class="text-danger">*</span>
                        </label>
                        <div class="col-lg-8 col-md-8 col-sm-12">
                            <select class="form-control select2" name="page_id" id="kt_select2_edit_menu"
                                    style="width: 100%;">
                                <option value="0"> Select page</option>
                                @foreach ($pages as $page)
                                    <option value="{{ $page->id }}"> {{ $page->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-12 control-label" for="status">{{ __('common.status') }} </label>
                        <div class="col-lg-6 col-md-8 col-sm-12">
                            <span class="switch switch-outline switch-icon switch-success">
                                <label>
                                    <input type="checkbox" name="status" id="status" value="1" checked="checked">
                                    <span></span>
                                </label>
                            </span>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" id="id" value="">
                    <button type="button" class="btn btn-default"
                            data-dismiss="modal">{{ trans('common.buttons.close') }}</button>
                    <button type="submit" class="btn btn-success">{{ trans('common.buttons.save') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
