<a href="{{ $buttonRoute }}"
   class="btn btn-success mr-3 {{ !empty($buttonType) ? 'btn-' . $buttonType : '' }} {{ !empty($class) ? $class : '' }}">
    <i class="flaticon2-plus mr-1"></i> {{ !empty($buttonNoLabel) ? '' : __('common.buttons.add_new') }}
</a>