<?php

namespace  App\Classes\Interfaces\Page;

use Illuminate\Http\Request;
use App\Models\Page\Page;

interface PageFactoryInterface
{
    public function get(int $id);

    public function store(Request $request);

    public function update(Request $request, int $id);

    public function delete(int $id): void;

    public function deletePublishQueue(Page $page): void;
}
