<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class UserLoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Sets custom validation messages.
     *
     * @return array
     */
    public function messages()
    {

        return [
            'email.required'    => __('validation.the_email_field_is_required'),
            'password.required' => __('validation.the_password_field_is_required'),
        ];

    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            'email'    => 'required|string|email|max:255',
            'password' => 'required|string|min:6',
        ];
    }
}
