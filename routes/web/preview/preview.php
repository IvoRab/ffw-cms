<?php

Route::group(
    [
        'namespace' => 'Preview',
        'as'        => 'Preview::',
    ],
    function () {
        Route::get('/{slug?}', [\App\Http\Controllers\Preview\PreviewController::class, 'index'])->name('index');
    }
);
