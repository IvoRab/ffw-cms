<a href="{{ $buttonRoute }}"
   class="btn btn-dark mr-3 {{ !empty($buttonType) ? 'btn-' . $buttonType : '' }}">
    <i class="flaticon2-refresh mr-1"></i> {{ !empty($buttonNoLabel) ? '' : __('common.buttons.resend_xml') }}
</a>