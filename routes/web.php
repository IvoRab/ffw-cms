<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [\App\Http\Controllers\Auth\AuthController::class, 'index'])->name('login');

// Dashboard routs
Route::prefix('/dashboard')->group(function () {
    foreach (File::allFiles(__DIR__ . '/web/dashboard') as $partial) {
        require $partial->getPathname();
    }
});

// Pages routes
Route::prefix('/pages')->group(function () {
    foreach (File::allFiles(__DIR__ . '/web/pages') as $partial) {
        require $partial->getPathname();
    }
});

// Menus routes
Route::prefix('/menus')->group(function () {
    foreach (File::allFiles(__DIR__ . '/web/menus') as $partial) {
        require $partial->getPathname();
    }
});

// Preview routes
Route::prefix('/preview')->group(function () {
    foreach (File::allFiles(__DIR__ . '/web/preview') as $partial) {
        require $partial->getPathname();
    }
});

// Publish routes
Route::prefix('/publish')->group(function () {
    foreach (File::allFiles(__DIR__ . '/web/publish') as $partial) {
        require $partial->getPathname();
    }
});
