<a href="{{ $buttonRoute }}"
   class="btn btn-warning mr-3 {{ !empty($buttonType) ? 'btn-' . $buttonType : '' }} {{ !empty($buttonTextCenter) ? 'text-center' : '' }} {{ !empty($class) ? $class : '' }}">
    <i class="flaticon2-pen mr-1"></i> {{ !empty($buttonNoLabel) ? '' : __('common.buttons.edit') }}
</a>