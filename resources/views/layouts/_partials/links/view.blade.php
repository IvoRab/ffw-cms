<a href="{{ $buttonRoute }}"
   class="btn btn-primary mr-3 {{ !empty($buttonType) ? 'btn-' . $buttonType : '' }}">
    <i class="flaticon-eye mr-1"></i> {{ !empty($buttonNoLabel) ? '' : __('common.buttons.view') }}
</a>
