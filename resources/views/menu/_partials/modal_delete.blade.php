<!-- Modal -->
<div class="modal fade" id="delete-menu-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <form action="{{ route('Menus::delete') }}"
                  method="post" class="form-horizontal"
                  enctype="multipart/form-data">
                <div class="modal-header">
                    <h4 class="modal-title">Delete menu</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>

                <div class="modal-body">
                    Are you sure you want to delete this menu element?
                </div>

                <div class="modal-footer">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" id="id" value="">
                    <button type="button" class="btn btn-default"
                            data-dismiss="modal">{{ trans('common.buttons.close') }}</button>
                    <button type="submit" class="btn btn-danger">{{ trans('common.buttons.delete') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
