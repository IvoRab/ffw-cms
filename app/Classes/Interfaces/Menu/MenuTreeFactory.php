<?php

namespace App\Classes\Interfaces\Menu;

use App\Models\Menu\Menu;

class MenuTreeFactory implements MenuTreeFactoryInterface
{
    /**
     * @param array $ids
     * @param bool  $activeOnly
     *
     * @return array
     */
    public function create(array $ids, bool $activeOnly = false): array
    {
        $menuTree = [];
        foreach ($ids as $id) {
            $menuEloquent = Menu::whereId($id);

            // Only active menu are required for the front site
            // we need all menu elements for the CRUD
            if ($activeOnly) {
                $menuEloquent->whereStatus(1);
            }
            $menu = $menuEloquent->first();

            if (!empty($menu)) {
                $menuTree[] = $this->createTree($menu, $activeOnly);
            }
        }

        return $menuTree;
    }

    /**
     * @param \App\Models\Menu\Menu $menu
     * @param bool                  $activeOnly
     *
     * @return array
     */
    protected function createTree(Menu $menu, bool $activeOnly): array
    {
        $node = $this->createNode($menu);
        foreach ($this->getSubMenus($menu->id, $activeOnly) as $subMenu) {
            $node['children'][] = $this->createTree($subMenu, $activeOnly);
        }

        return $node;
    }

    /**
     * @param int  $id
     * @param bool $activeOnly
     *
     * @return \App\Models\Menu\Menu[]|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    protected function getSubMenus(int $id, bool $activeOnly)
    {
        $menusEloquent = Menu::whereParentMenuId($id);
        if ($activeOnly) {
            $menusEloquent->whereStatus(1);
        }

        return $menusEloquent->orderBy('position')->get();
    }

    /**
     * @param \App\Models\Menu\Menu $menu
     *
     * @return array
     */
    protected function createNode(Menu $menu): array
    {
        $page = $menu->page;

        return [
            'id'           => $menu->id,
            'is_home_menu' => $menu->is_home_menu,
            'name'         => $menu->name,
            'page_id'      => !empty($page->id) ? $menu->page->id : '',
            'page'         => !empty($page->id) ? $menu->page->name : '',
            'status'       => $menu->status,
            'children'     => [],
        ];
    }

    /**
     * @param     $menus
     * @param int $parentId
     */
    public function update($menus, int $parentId = 0): void
    {
        foreach ($menus as $position => $menu) {
            $menuElement = Menu::whereId($menu->id)->first();
            $menuElement->update(
                [
                    'parent_menu_id' => $parentId,
                    'position'       => $position + 1,
                ]
            );

            // check if the current element have children and if it's an array of objects
            // in some cases the value of $menu->children has the value of 1
            if (!empty($menu->children) && is_array($menu->children)) {
                $this->update($menu->children, $menu->id);
            }
        }
    }

    /**
     * @return string
     */
    public function getHomePageUrl(): string
    {
        //
    }
}
