<a href="{{ $buttonRoute }}"
   class="btn btn-secondary mr-3 {{ !empty($buttonType) ? 'btn-' . $buttonType : '' }} {{ !empty($class) ? $class : '' }}">
    <i class="flaticon2-delete mr-1"></i> {{ !empty($buttonNoLabel) ? '' : __('common.buttons.cancel') }}
</a>