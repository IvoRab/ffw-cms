<?php

namespace App\Classes\Interfaces\Menu;

use Illuminate\Http\Request;

interface MenuFactoryInterface
{
    public function store(Request $request);

    public function update(Request $request, $id);

    public function delete($id): void;
}
