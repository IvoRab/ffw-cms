var FFWCmsAuthApp = {
    conf: {
        debug: true,
        page_name: "",
    },

    init: function () {
        var self = this;

        self.setPageName();

        if (self.conf.page_name === "login") {
            $(document).on('click', '#kt_submit_login', function (event) {
                event.preventDefault();
                event.stopPropagation();

                self.login($('#email').val(), $('#password').val(), $('form').data("action"));
            });
        }

        if (self.conf.page_name === "refresh-token") {
            $(document).on('click', '#kt_submit_refresh_token', function (event) {
                event.preventDefault();
                event.stopPropagation();

                self.refreshToken($('form').data("action"));
            });
        }

        $(document).on('click', '.js-logout-btn', function (event) {
            event.preventDefault();
            event.stopPropagation();

            self.logout($(this).data("url"));
        });
    },

    login: function (email, password, url) {
        var self = this;

        $.ajax({
            url: url,
            method: 'POST',
            dataType: 'json',
            data: {email: email, password: password},
            success: function (data) {
                window.location.href = data.redirect_url;
            },
            complete: function (xhr, textStatus) {
                if (textStatus === "error") {
                    $('#login_error_message').removeClass('d-none');
                }
            }
        });
    },

    refreshToken: function (url) {
        var self = this;

        $.ajax({
            url: url,
            method: 'POST',
            dataType: 'json',
            success: function (data) {
                window.location.href = data.redirect_url;
            }
        });
    },

    logout: function (url) {
        var self = this;

        $.ajax({
            url: url,
            method: 'POST',
            dataType: 'json',
            success: function (data) {
                window.location.href = data.redirect_url;
            }
        });
    },

    setPageName: function () {
        var self = this;

        self.conf.page_name = $('body').data("page-name");
    },

    logData: function (text, variable) {
        var self = this;
        var value = variable || null;

        if (self.conf.debug && window.console) {
            if (value) {
                console.log(text, variable);
            } else {
                console.log(text);
            }
        }
    }
};

$(document).ready(function () {
    console.log('authentication JS Loaded');

    FFWCmsAuthApp.init();
});
