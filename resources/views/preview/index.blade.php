<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ $page->meta_title ?? env('APP_NAME') }}</title>
    <meta name="description" content="{{ $page->meta_description ?? "" }}">
    <meta name="keywords" content="{{ implode(', ', $keywords) }}">

    <!-- Bootstrap core CSS -->
    <link href="/cms/css/bootstrap.min.css" rel="stylesheet" crossorigin="anonymous">

    <!-- Favicons -->
    <link rel="icon" href="/cms/img/favicon.ico">
    <meta name="theme-color" content="#7952b3">

    <style type="text/css">
        a.navbar-brand img {
            max-height: 40px;
        }
    </style>
</head>
<body>

<main class="container">
    <nav class="navbar navbar-expand-md navbar-dark bg-dark ">
        <div class="container-fluid">
            <a class="navbar-brand" href="{{ $homePageUrl }}"><img src="/cms/img/logo.png" alt="CMS"></a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse"
                    aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav col-12 col-lg-auto me-lg-auto mb-2 justify-content-left mb-md-0">
                    @foreach($menus as $menu)
                        @if(!empty($menu['children']))
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="javascript:;" id="{{ $menu['id'] }}" data-bs-toggle="dropdown"
                                   aria-haspopup="true" aria-expanded="false">{{ $menu['name'] }}</a>

                                @include('preview._partials.menu', ['menus' => $menu['children']])
                            </li>
                        @else
                            <li class="nav-item">
                                <a class="nav-link" href="{{ !empty($menu['url']) ? $menu['url'] : 'javascript:;' }}">
                                    {{ $menu['name'] }}
                                </a>
                            </li>
                        @endif

                    @endforeach
                </ul>
            </div>
        </div>
    </nav>

    <div class="bg-light p-5 rounded">
        <h1>{{ $page->title ?? '' }}</h1>

        <p class="lead">{!! $page->body !!}</p>
    </div>
</main>

<script src="/cms/js/popper.min.js"></script>
<script src="/cms/js/bootstrap.bundle.min.js"></script>

<script type="text/javascript">
    (function ($bs) {
        const CLASS_NAME = 'has-child-dropdown-show';
        $bs.Dropdown.prototype.toggle = function (_orginal) {
            return function () {
                document.querySelectorAll('.' + CLASS_NAME).forEach(function (e) {
                    e.classList.remove(CLASS_NAME);
                });
                let dd = this._element.closest('.dropdown').parentNode.closest('.dropdown');
                for (; dd && dd !== document; dd = dd.parentNode.closest('.dropdown')) {
                    dd.classList.add(CLASS_NAME);
                }
                return _orginal.call(this);
            }
        }($bs.Dropdown.prototype.toggle);

        document.querySelectorAll('.dropdown').forEach(function (dd) {
            dd.addEventListener('hide.bs.dropdown', function (e) {
                if (this.classList.contains(CLASS_NAME)) {
                    this.classList.remove(CLASS_NAME);
                    e.preventDefault();
                }
                e.stopPropagation(); // do not need pop in multi level mode
            });
        });

        // for hover
        document.querySelectorAll('.dropdown-hover, .dropdown-hover-all .dropdown').forEach(function (dd) {
            dd.addEventListener('mouseenter', function (e) {
                let toggle = e.target.querySelector(':scope>[data-bs-toggle="dropdown"]');
                if (!toggle.classList.contains('show')) {
                    $bs.Dropdown.getOrCreateInstance(toggle).toggle();
                    dd.classList.add(CLASS_NAME);
                    $bs.Dropdown.clearMenus();
                }
            });
            dd.addEventListener('mouseleave', function (e) {
                let toggle = e.target.querySelector(':scope>[data-bs-toggle="dropdown"]');
                if (toggle.classList.contains('show')) {
                    $bs.Dropdown.getOrCreateInstance(toggle).toggle();
                }
            });
        });
    })(bootstrap);
</script>

</body>
</html>
