<button type="button" class="btn btn-warning mr-3 js-protocol-object-closed"
        data-btn-yes="{{ __('common.yes') }}" data-btn-no="{{ __('common.no') }}"
        data-title="ПРОТОКОЛ № {{ $order->order_number }}"
        data-text="{{ __('pages/orders.protocol_object_no_access_confirmation') }}">
    <i class="flaticon2-warning mr-1"></i> {{ __('common.buttons.object_no_access') }}
</button>