<a href="{{ $buttonRoute }}"
   class="btn btn-success mr-3 {{ !empty($buttonType) ? 'btn-' . $buttonType : '' }} {{ !empty($buttonTextCenter) ? 'text-center' : '' }}">
    <i class="flaticon2-download mr-1"></i> {{ !empty($buttonNoLabel) ? '' : __('common.buttons.download') }}
</a>
