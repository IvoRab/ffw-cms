<?php

namespace App\Classes\Interfaces\Publisher;

use App\Classes\Interfaces\Menu\MenuTreeFactoryInterface;
use Illuminate\Support\Facades\Storage;
use App\Classes\Helpers\PublishHelper;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;
use App\Classes\Traits\MenusTrait;
use App\Classes\Traits\PagesTrait;
use App\Models\Page\PublishQueue;
use App\Models\Page\Page;
use ZipArchive;
use Exception;

class ArchivePublisher implements PublisherInterface
{
    use MenusTrait, PagesTrait;

    private $pages;
    private $menus;
    private $filename = 'cms.zip';
    private $homePageUrl;

    /** @var MenuTreeFactoryInterface $menuTreeFactory */
    private $menuTreeFactory;

    /**
     * ArchivePublisher constructor.
     *
     * @param \App\Classes\Interfaces\Menu\MenuTreeFactoryInterface $menuTreeFactory
     */
    public function __construct(MenuTreeFactoryInterface $menuTreeFactory)
    {
        $this->setMenuTreeFactory($menuTreeFactory);
        $this->setMenus($this->getMenuTreeFactory()->create($this->getMainMenuIds(), true));
        $this->setHomePageUrl($this->getMenuTreeFactory()->getHomePageUrl());
    }

    /**
     * @throws \Exception
     */
    public function publish(): void
    {
        try {
            DB::beginTransaction();

            $this->setPages();
            $this->createAssets();

            if (!empty($this->getPages())) {
                foreach ($this->getPages() as $page) {
                    $this->createPage($this->getMenus(), $page, $this->getPageKeywords($page), $this->getHomePageUrl());
                }
            }

            $this->archive();

            DB::commit();
        } catch (Exception $exception) {
            // In case of error, rollback the deleted publish queue
            DB::rollBack();

            throw new Exception($exception->getMessage());
        }
    }

    /**
     * @param $menus
     * @param $page
     * @param $keywords
     * @param $mainUrl
     *
     * @return string
     */
    protected function renderPage($menus, $page, $keywords, $mainUrl): string
    {
        return view(
            'preview.index',
            [
                'menus'       => $menus,
                'page'        => $page,
                'keywords'    => $keywords,
                'homePageUrl' => $mainUrl,
            ]
        )->render();
    }

    /**
     * Create the publish folder and the assets
     */
    public function createAssets(): void
    {
        // Delete the whole folder if it exists to avoid export of old files
        if (Storage::exists(PublishHelper::getArchiveDirectory())) {
            Storage::deleteDirectory(PublishHelper::getArchiveDirectory());
        }

        // Delete the previous archive file
        if (Storage::exists($this->getArchivePath(false))) {
            Storage::delete($this->getArchivePath(false));
        }

        // Copy the assets (images, css and js) into the publish folder
        File::copyDirectory(public_path('cms/'), storage_path('app/' . PublishHelper::getArchiveDirectory() . '/cms'));
    }

    /**
     * @param $menus
     * @param $page
     * @param $keywords
     * @param $mainUrl
     */
    public function createPage($menus, $page, $keywords, $mainUrl): void
    {
        $pageContent = $this->renderPage($menus, $page, $keywords, $mainUrl);
        $filename    = $this->getMenuTreeFactory()->getStaticPageUrl($page->seo_url);

        File::put(storage_path('app/' . PublishHelper::getArchiveDirectory() . '/' . $filename), $pageContent);
    }

    public function upload(): void
    {
        //
    }

    /**
     * Archive the published pages
     */
    public function archive(): void
    {
        try {
            $zip = new ZipArchive();

            if ($zip->open($this->getArchivePath(), ZipArchive::CREATE)) {
                $files = Storage::allFiles(PublishHelper::getArchiveDirectory());

                foreach ($files as $file) {
                    $path     = storage_path('app/' . $file);
                    $filename = str_replace('publish/archive/', '', $file);
                    $zip->addFile($path, $filename);
                }

                $zip->close();
            }
        } catch (Exception $exception) {
            throw new Exception($exception->getMessage());
        }
    }

    /**
     * Set pages in publish queue
     */
    private function setPages()
    {
        $queuedPages = PublishQueue::pluck('page_id')->toArray();

        // Clear publish queue immediately after selecting the queued pages
        $this->clearPublishQueue();

        if (!empty($queuedPages)) {
            $this->pages = Page::whereIn('id', $queuedPages)->get();
        }
    }

    /**
     * @return mixed
     */
    private function getPages()
    {
        return $this->pages;
    }

    /**
     * Set menu tree factory
     */
    private function setMenuTreeFactory(MenuTreeFactoryInterface $menuTreeFactory)
    {
        $this->menuTreeFactory = $menuTreeFactory;
    }

    /**
     * @return \App\Classes\Interfaces\Menu\MenuTreeFactoryInterface
     */
    private function getMenuTreeFactory(): \App\Classes\Interfaces\Menu\MenuTreeFactoryInterface
    {
        return $this->menuTreeFactory;
    }

    /**
     * Set menu tree
     */
    private function setMenus($menus)
    {
        $this->menus = $menus;
    }

    /**
     * @return mixed
     */
    private function getMenus()
    {
        return $this->menus;
    }

    /**
     * Set menu tree
     */
    private function setHomePageUrl(string $url)
    {
        $this->homePageUrl = $url;
    }

    /**
     * @return string
     */
    private function getHomePageUrl(): string
    {
        return $this->homePageUrl;
    }

    /**
     * Delete the publish queue
     */
    private function clearPublishQueue()
    {
        PublishQueue::truncate();
    }

    /**
     * @return string
     */
    public function getFilename(): string
    {
        return $this->filename;
    }

    /**
     * @param bool $fullPath
     *
     * @return string
     */
    public function getArchivePath(bool $fullPath = true): string
    {
        return PublishHelper::getPublishDirectory($fullPath) . DIRECTORY_SEPARATOR . $this->getFilename();
    }
}
