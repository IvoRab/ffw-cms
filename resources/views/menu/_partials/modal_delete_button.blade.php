<button class="btn btn-danger btn-xs mr-3 delete-menu-btn" data-toggle="modal"
        data-target="#delete-menu-modal" data-id="{{ $id }}">
    <i class="flaticon2-rubbish-bin-delete-button ml-1"></i>
</button>
