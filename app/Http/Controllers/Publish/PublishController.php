<?php

namespace App\Http\Controllers\Publish;

use App\Classes\Interfaces\Menu\MenuPublishTree;
use App\Classes\Interfaces\Publisher\ArchivePublisher;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;

class PublishController extends Controller
{
    /**
     * PublishController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth:api');

        $this->setPageTitle(__('common.sidebar.publish'));
        $this->setPageName('Publish');

        parent::__construct();
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $logo    = !File::exists(public_path('cms/img/logo.png')) ?: asset('cms/img/logo.png');
        $favicon = !File::exists(public_path('cms/img/favicon.ico')) ?: asset('cms/img/favicon.ico');

        return view(
            'publish.index',
            [
                'logo'    => $logo,
                'favicon' => $favicon,
            ]
        );
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        if ($request->hasFile('logo')) {
            $request->file('logo')->move(public_path('cms/img'), 'logo.png');
        }

        if ($request->hasFile('favicon')) {
            $request->file('favicon')->move(public_path('cms/img'), 'favicon.ico');
        }

        return redirect()
            ->route('Publish::index')
            ->with(
                [
                    'messages' => [
                        'success' =>
                            'Files uploaded Successfully!',
                    ],
                ]
            );
    }

    /**
     * @return \Illuminate\Http\RedirectResponse|\Symfony\Component\HttpFoundation\BinaryFileResponse
     * @throws \Exception
     */
    public function download()
    {
        $publisher = new ArchivePublisher(new MenuPublishTree());
        $publisher->publish();

        $archive = $publisher->getArchivePath();
        if (!File::exists($archive)) {
            return redirect()
                ->route('Publish::index')
                ->with(
                    [
                        'messages' => [
                            'danger' =>
                                'An error occurred while creating the archive!',
                        ],
                    ]
                );
        }

        return response()->download($publisher->getArchivePath());
    }
}
