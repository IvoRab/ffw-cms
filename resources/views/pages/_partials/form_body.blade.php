<div class="form-group">
    <label class="col-12 control-label" for="name">
        Name <span class="text-danger">*</span>
    </label>
    <div class="col-lg-8 col-md-8 col-sm-12">
        <input type="text" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}"
               placeholder="Page name" name="name" id="name"
               value="{{ old('name', $page->name ?? '') }}">


        @if ($errors->has('name'))
            <div class="fv-plugins-message-container">
                <div data-field="name" data-validator="notEmpty" class="fv-help-block">
                    {{ $errors->first('name') }}
                </div>
            </div>
        @endif
    </div>
</div>

<div class="form-group">
    <label class="col-12 control-label" for="title">
        Title <span class="text-danger">*</span>
    </label>
    <div class="col-lg-8 col-md-8 col-sm-12">
        <input type="text" class="form-control {{ $errors->has('title') ? 'is-invalid' : '' }}"
               placeholder="Page title" name="title" id="title"
               value="{{ old('title', $page->title ?? '') }}">


        @if ($errors->has('title'))
            <div class="fv-plugins-message-container">
                <div data-field="title" data-validator="notEmpty" class="fv-help-block">
                    {{ $errors->first('title') }}
                </div>
            </div>
        @endif
    </div>
</div>

<div class="form-group">
    <label class="col-12 control-label" for="body">
        Body <span class="text-danger">*</span>
    </label>
    <div class="col-lg-8 col-md-8 col-sm-12">
        <textarea class="form-control {{ $errors->has('body') ? 'is-invalid' : '' }}" name="body" id="body"
                  cols="30" rows="10">{{ old('body', $page->body ?? '') }}</textarea>

        @if ($errors->has('body'))
            <div class="fv-plugins-message-container">
                <div data-field="body" data-validator="notEmpty" class="fv-help-block">
                    {{ $errors->first('body') }}
                </div>
            </div>
        @endif
    </div>
</div>

<div class="form-group">
    <label class="col-12 control-label" for="meta_title">Meta title</label>
    <div class="col-lg-8 col-md-8 col-sm-12">
        <input type="text" class="form-control {{ $errors->has('meta_title') ? 'is-invalid' : '' }}"
               placeholder="Meta title" name="meta_title" id="meta_title"
               value="{{ old('meta_title', $page->meta_title ?? '') }}">


        @if ($errors->has('meta_title'))
            <div class="fv-plugins-message-container">
                <div data-field="meta_title" data-validator="notEmpty" class="fv-help-block">
                    {{ $errors->first('meta_title') }}
                </div>
            </div>
        @endif
    </div>
</div>

<div class="form-group">
    <label class="col-12 control-label" for="meta_description">Meta description</label>
    <div class="col-lg-8 col-md-8 col-sm-12">
        <input type="text" class="form-control {{ $errors->has('meta_description') ? 'is-invalid' : '' }}"
               placeholder="Meta description" name="meta_description" id="meta_description"
               value="{{ old('meta_description', $page->meta_description ?? '') }}">


        @if ($errors->has('meta_description'))
            <div class="fv-plugins-message-container">
                <div data-field="meta_description" data-validator="notEmpty" class="fv-help-block">
                    {{ $errors->first('meta_description') }}
                </div>
            </div>
        @endif
    </div>
</div>

<div class="form-group">
    <label class="col-12 control-label" for="keywords">Meta keywords</label>
    <div class="col-lg-8 col-md-8 col-sm-12">
        <input id="keywords" class="form-control tagify" name='keywords' placeholder='Meta keywords'
               value='{{ !empty($pageKeyword) ? implode(', ', $pageKeyword) : '' }}' data-blacklist=''/>

        <div class="mt-3">
            <a href="javascript:;" id="keywords_remove" class="btn btn-sm btn-light-primary font-weight-bold">
                Remove keywords
            </a>
        </div>
    </div>
</div>

<div class="form-group">
    <label class="col-12 control-label" for="seo_url">SEO URL</label>
    <div class="col-lg-8 col-md-8 col-sm-12">
        <input type="text" class="form-control {{ $errors->has('seo_url') ? 'is-invalid' : '' }}"
               placeholder="SEO URL" name="seo_url" id="seo_url" {{ !empty($page->is_home_page) ? 'disabled' : '' }}
               value="{{ old('seo_url', $page->seo_url ?? '') }}">


        @if ($errors->has('seo_url'))
            <div class="fv-plugins-message-container">
                <div data-field="seo_url" data-validator="notEmpty" class="fv-help-block">
                    {{ $errors->first('seo_url') }}
                </div>
            </div>
        @endif
    </div>
</div>
