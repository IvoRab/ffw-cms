<button type="submit" class="btn btn-success mr-3 {{ !empty($class) ? $class : '' }} ">
    <i class="flaticon2-check-mark mr-1"></i> {{ trans('common.buttons.save') }}
</button>