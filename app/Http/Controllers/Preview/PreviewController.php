<?php

namespace App\Http\Controllers\Preview;

use App\Classes\Interfaces\Menu\MenuPreviewTree;
use App\Http\Controllers\Controller;
use App\Classes\Traits\MenusTrait;
use App\Classes\Traits\PagesTrait;
use App\Models\Page\Page;

class PreviewController extends Controller
{
    use MenusTrait, PagesTrait;

    /**
     * PreviewController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth:api');

        parent::__construct();
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index($slug = '')
    {
        $menuTreeFactory = new MenuPreviewTree();
        $menus           = $menuTreeFactory->create($this->getMainMenuIds(), true);

        $page = Page::whereSeoUrl($slug)->firstOrFail();

        return view(
            'preview.index',
            [
                'menus'       => $menus,
                'page'        => $page,
                'keywords'    => $this->getPageKeywords($page),
                'homePageUrl' => $menuTreeFactory->getHomePageUrl(),
            ]
        );
    }
}
