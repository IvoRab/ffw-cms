<?php

namespace App\Classes\Helpers;

class PublishHelper
{
    const DIR_PUBLISH = 'publish';
    const DIR_ARCHIVE = 'archive';

    /**
     * @param bool $fullPath
     *
     * @return string
     */
    public static function getPublishDirectory(bool $fullPath = false): string
    {
        return $fullPath ? storage_path('app' . DIRECTORY_SEPARATOR . self::DIR_PUBLISH) : self::DIR_PUBLISH;
    }

    /**
     * @param bool $fullPath
     *
     * @return string
     */
    public static function getArchiveDirectory(bool $fullPath = false): string
    {
        return $fullPath ?
            storage_path('app' . DIRECTORY_SEPARATOR . self::DIR_PUBLISH . DIRECTORY_SEPARATOR . self::DIR_ARCHIVE) :
            self::DIR_PUBLISH . DIRECTORY_SEPARATOR . self::DIR_ARCHIVE;
    }
}
