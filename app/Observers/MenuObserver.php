<?php

namespace App\Observers;

use App\Classes\Traits\PagesTrait;
use App\Models\Menu\Menu;

class MenuObserver
{
    use PagesTrait;

    /**
     * Handle the Menu "saved" event.
     *
     * @param \App\Models\Menu\Menu $menu
     *
     * @return void
     */
    public function saved(Menu $menu)
    {
        $this->queueAllPages();
    }

    /**
     * Handle the Menu "deleted" event.
     *
     * @param \App\Models\Menu\Menu $menu
     *
     * @return void
     */
    public function deleted(Menu $menu)
    {
        $this->queueAllPages();
    }
}
