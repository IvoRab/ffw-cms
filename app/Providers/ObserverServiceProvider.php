<?php

namespace App\Providers;

use App\Models\Menu\Menu;
use App\Models\Page\Page;
use App\Observers\MenuObserver;
use App\Observers\PageObserver;

class ObserverServiceProvider extends AppServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Page::observe(PageObserver::class);
        Menu::observe(MenuObserver::class);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
