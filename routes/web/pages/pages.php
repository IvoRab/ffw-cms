<?php

Route::group(
    [
        'namespace' => 'Page',
        'as'        => 'Pages::',
    ],
    function () {
        Route::get('/', [\App\Http\Controllers\Page\PagesController::class, 'index'])->name('index');
        Route::post('ajax/grid', [\App\Http\Controllers\Page\PagesController::class, 'gridData'])->name('gridData');
        Route::get('create', [\App\Http\Controllers\Page\PagesController::class, 'create'])->name('create');
        Route::post('store', [\App\Http\Controllers\Page\PagesController::class, 'store'])->name('store');
        Route::get('view/{id}', [\App\Http\Controllers\Page\PagesController::class, 'view'])->name('view');
        Route::get('edit/{id}', [\App\Http\Controllers\Page\PagesController::class, 'edit'])->name('edit');
        Route::post('update/{id}', [\App\Http\Controllers\Page\PagesController::class, 'update'])->name('update');
        Route::post('delete/{id}', [\App\Http\Controllers\Page\PagesController::class, 'delete'])->name('delete');

        Route::post('/upload-media', [\App\Http\Controllers\Page\PagesController::class, 'upload'])->name('upload');
    }
);
