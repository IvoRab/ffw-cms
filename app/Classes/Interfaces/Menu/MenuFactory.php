<?php

namespace App\Classes\Interfaces\Menu;

use Illuminate\Http\Request;
use App\Models\Menu\Menu;
use Exception;

class MenuFactory implements MenuFactoryInterface
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return mixed
     * @throws \Exception
     */
    public function store(Request $request)
    {
        try {
            return Menu::create(
                [
                    'parent_menu_id' => $request->get('parent_menu_id', 0),
                    'page_id'        => $request->get('page_id', 0),
                    'name'           => $request->get('name'),
                    'position'       => $this->getPosition($request->get('parent_menu_id', 0)),
                    'status'         => $request->has('status'),
                ]
            );
        } catch (Exception $exception) {
            throw new Exception($exception->getMessage());
        }
    }

    /**
     * @param $parentMenuId
     *
     * @return int
     */
    private function getPosition($parentMenuId): int
    {
        return Menu::whereParentMenuId($parentMenuId)->count() + 1;
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param                          $id
     *
     * @return \App\Models\Menu\Menu|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
     * @throws \Exception
     */
    public function update(Request $request, $id)
    {
        try {
            $menu = Menu::whereId($id)->firstOrFail();
            $menu->update(
                [
                    'page_id' => $request->get('page_id', 0),
                    'name'    => $request->get('name'),
                    'status'  => $request->has('status'),
                ]
            );

            return $menu;
        } catch (Exception $exception) {
            throw new Exception($exception->getMessage());
        }
    }

    /**
     * @param $id
     */
    public function delete($id): void
    {
        $menu = Menu::whereId($id)->firstOrFail();
        $menu->delete();
    }
}
