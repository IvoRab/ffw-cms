<?php

namespace App\Classes\Traits;

use App\Models\Menu\Menu;

trait MenusTrait
{
    /**
     * @return \App\Models\Menu\Menu[]|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getMainMenus()
    {
        return Menu::whereParentMenuId(0)->orderBy('position')->get();
    }

    /**
     * @return array
     */
    public function getMainMenuIds(): array
    {
        $ids = [];
        foreach ($this->getMainMenus() as $menu) {
            $ids[] = $menu->id;
        }

        return $ids;
    }
}
