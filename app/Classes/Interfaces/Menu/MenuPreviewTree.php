<?php

namespace App\Classes\Interfaces\Menu;

use App\Models\Menu\Menu;
use App\Models\Page\Page;

class MenuPreviewTree extends MenuTreeFactory
{
    /**
     * Override create node method to add the url prefix "preview"
     *
     * @param \App\Models\Menu\Menu $menu
     *
     * @return array
     */
    protected function createNode(Menu $menu): array
    {
        $page = $menu->page;

        return [
            'id'       => $menu->id,
            'name'     => $menu->name,
            'page'     => !empty($page->id) ? $menu->page->name : '',
            'url'      => !empty($page->id) ?
                route('Preview::index', ['slug' => ($page->seo_url)]) : route('Preview::index'),
            'children' => [],
        ];
    }

    /**
     * @return string
     */
    public function getHomePageUrl(): string
    {
        $page = Page::whereIsHomePage(1)->firstOrFail();

        return route('Preview::index', ['slug' => $page->seo_url]);
    }
}
