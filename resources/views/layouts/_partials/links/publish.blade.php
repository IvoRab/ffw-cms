<a href="{{ $buttonRoute }}" class="btn btn-success mr-3">
    <i class="flaticon-download-1  mr-1"></i> {{ __('common.buttons.publish') }}
</a>
