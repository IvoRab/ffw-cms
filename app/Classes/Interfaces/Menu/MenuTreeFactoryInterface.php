<?php

namespace App\Classes\Interfaces\Menu;

interface MenuTreeFactoryInterface
{
    public function create(array $ids, bool $activeOnly = false): array;

    public function update($menus, int $parentId = 0): void;

    public function getHomePageUrl(): string;
}
