<?php

namespace App\Classes\Interfaces\Menu;

use App\Models\Menu\Menu;
use App\Models\Page\Page;

class MenuPublishTree extends MenuTreeFactory
{
    /**
     * Override create node method to add the url prefix "preview"
     *
     * @param \App\Models\Menu\Menu $menu
     *
     * @return array
     */
    protected function createNode(Menu $menu): array
    {
        $seoUrl = empty($menu->page->seo_url) ? '' : $menu->page->seo_url;

        return [
            'id'       => $menu->id,
            'name'     => $menu->name,
            'url'      => $this->getStaticPageUrl($seoUrl),
            'children' => [],
        ];
    }

    /**
     * @param $seoUrl
     *
     * @return string
     */
    public function getStaticPageUrl($seoUrl): string
    {
        return '/' . (empty($seoUrl) ? 'index' : $seoUrl) . '.html';
    }

    /**
     * @return string
     */
    public function getHomePageUrl(): string
    {
        $page = Page::whereIsHomePage(1)->firstOrFail();

        return $this->getStaticPageUrl($page->seo_url);
    }
}
