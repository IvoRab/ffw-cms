<div class="dropdown-menu" aria-labelledby="">
    @foreach($menus as $menu)
        @if(!empty($menu['children']))
            <div class="dropdown dropend">
                <a class="dropdown-item dropdown-toggle" href="javascript:;" id="dropdown-layouts{{ $menu['id'] }}"
                   data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ $menu['name'] }}</a>

                @include('preview._partials.menu', ['menus' => $menu['children']])
            </div>
        @else
            <a class="dropdown-item" href="{{ !empty($menu['url']) ? $menu['url'] : 'javascript:;' }}">
                {{ $menu['name'] }}
            </a>
        @endif
    @endforeach
</div>
