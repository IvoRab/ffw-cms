@extends('layouts.app')

@section('content')
    <!--begin::Card-->
    <div class="row">
        <div class="col-md-12">
            <div class="card card-custom">
                <div class="card-header flex-wrap pt-4 pb-4">
                    <div class="card-title">
                        <h3 class="card-label">
                            {{ !empty($pageTitle) ? $pageTitle : '' }}
                        </h3>
                    </div>

                    <div class="card-toolbar">
                        @include('layouts._partials.links.preview', ['buttonRoute' => route('Preview::index')])
                        @include('layouts._partials.links.download', ['buttonRoute' => route('Publish::download')])
                    </div>
                </div>

                <!--begin: Form-->
                <form action="{{ route('Publish::store') }}" method="post"
                      class="form-horizontal" enctype="multipart/form-data">

                    <div class="card-body">
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label text-right">Logo</label>
                            <div class="col-lg-9">
                                <div class="image-input image-input-outline" id="kt_logo">
                                    <div class="image-input-wrapper"
                                         style="background-image: url({{ $logo }})"></div>

                                    <label
                                        class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow"
                                        data-action="change" data-toggle="tooltip" title=""
                                        data-original-title="Change logo">
                                        <i class="fa fa-pen icon-sm text-muted"></i>
                                        <input type="file" name="logo" accept=".png"/>
                                        <input type="hidden" name="logo_remove"/>
                                    </label>

                                    <span
                                        class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow"
                                        data-action="cancel" data-toggle="tooltip" title="Cancel logo">
                                    <i class="ki ki-bold-close icon-xs text-muted"></i>
                                </span>
                                    <span class="form-text text-muted">Allowed file types: png</span>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label text-right">Favicon</label>
                            <div class="col-lg-9">
                                <div class="image-input image-input-outline" id="kt_favicon">
                                    <div class="image-input-wrapper"
                                         style="background-image: url({{ $favicon }})"></div>

                                    <label
                                        class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow"
                                        data-action="change" data-toggle="tooltip" title=""
                                        data-original-title="Change favicon">
                                        <i class="fa fa-pen icon-sm text-muted"></i>
                                        <input type="file" name="favicon" accept=".png"/>
                                        <input type="hidden" name="favicon_remove"/>
                                    </label>

                                    <span
                                        class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow"
                                        data-action="cancel" data-toggle="tooltip" title="Cancel favicon">
                                    <i class="ki ki-bold-close icon-xs text-muted"></i>
                                </span>
                                    <span class="form-text text-muted">Allowed file types: ico</span>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="card-footer text-center">
                        {{ csrf_field() }}
                        @include('layouts._partials.buttons.save')
                        @include('layouts._partials.links.cancel', ['buttonRoute' => route('Publish::index')])
                    </div>

                </form>
                <!--end: Form-->

            </div>
        </div>
    </div>
    <!--end::Card-->
@endsection
