<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTablePages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('pages');

        Schema::create('pages', function (Blueprint $table) {
            $table->id();
            $table->tinyInteger('is_home_page')->unsigned()->default(0);
            $table->string('name');
            $table->string('title');
            $table->longText('body');
            $table->string('meta_title')->nullable();
            $table->string('meta_description')->nullable();
            $table->string('seo_url');
            $table->timestamps();
        });

        \Illuminate\Support\Facades\DB::table('pages')->insert(
            [
                'is_home_page'     => 1,
                'name'             => 'Home page',
                'title'            => 'FFW CMS',
                'body'             => '',
                'meta_title'       => '',
                'meta_description' => '',
                'seo_url'          => '',
                'created_at'        => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at'       => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('pages');
    }
}
