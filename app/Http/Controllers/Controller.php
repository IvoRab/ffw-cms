<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\View;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    private   $pageName    = '';
    private   $pageTitle   = '';
    protected $dataTableId = '';

    /** @var Log $logger */
    private $logger;

    /**
     * Controller constructor.
     */
    public function __construct()
    {
        View::share('pageTitle', $this->getPageTitle());
        View::share('pageName', $this->getPageName());
        \View::share('dateTableId', $this->dataTableId);
        $this->setLogger();
    }

    /**
     * @return \Illuminate\Contracts\Auth\Guard|\Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function getGuard()
    {
        return Auth::guard();
    }

    /**
     * @param string $name
     */
    protected function setPageName(string $name)
    {
        View::share('pageName', $name);
        $this->pageName = $name;
    }

    /**
     * @return string
     */
    protected function getPageName(): string
    {
        return $this->pageName;
    }

    /**
     * @param string $name
     */
    protected function setPageTitle(string $name)
    {
        View::share('pageTitle', $name);
        $this->pageTitle = $name;
    }

    /**
     * @return string
     */
    protected function getPageTitle(): string
    {
        return $this->pageTitle;
    }

    /**
     * Set error logger
     */
    protected function setLogger()
    {
        $this->logger = Log::channel('CMSErrorLog');
    }

    /**
     * @return \Illuminate\Support\Facades\Log
     */
    protected function getLogger(): \Illuminate\Support\Facades\Log
    {
        return $this->logger;
    }

    /**
     * @return mixed
     */
    public function getDataTableId()
    {
        return $this->dataTableId;
    }

    /**
     * @param mixed $dataTableId
     */
    public function setDataTableId($dataTableId)
    {
        $this->dataTableId = $dataTableId;
    }
}
