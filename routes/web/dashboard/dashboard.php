<?php

Route::group(
    [
        'namespace' => 'Dashboard',
        'as'        => 'Dashboard::',
    ],
    function () {
        Route::get('/', [\App\Http\Controllers\Dashboard\DashboardController::class, 'index'])->name('index');
    }
);
