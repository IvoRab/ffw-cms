<?php

namespace App\Models\Page;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Page\PublishQueue
 *
 * @property int $id
 * @property int $page_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Page\Page $page
 * @method static \Illuminate\Database\Eloquent\Builder|PublishQueue newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PublishQueue newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PublishQueue query()
 * @method static \Illuminate\Database\Eloquent\Builder|PublishQueue whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PublishQueue whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PublishQueue wherePageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PublishQueue whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class PublishQueue extends Model
{
    protected $table = 'page_publish_queues';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'page_id',
    ];

    public function page()
    {
        return $this->belongsTo(Page::class);
    }
}
