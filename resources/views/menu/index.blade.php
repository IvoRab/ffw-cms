@extends('layouts.app')

@section('content')
    <!--begin::Card-->
    <div class="card card-custom">
        <div class="card-header flex-wrap pt-4 pb-4">
            <div class="card-title">
                <h3 class="card-label">
                    {{ !empty($pageTitle) ? $pageTitle : '' }}
                </h3>
            </div>

            <div class="card-toolbar" id="nestable-menu">
                <button type="button" data-action="expand-all" class="btn btn-default mr-3">
                    <i class="fas fa-expand-alt"></i> Expand All
                </button>
                <button type="button" data-action="collapse-all" class="btn btn-default mr-3">
                    <i class="fas fa-compress-alt"></i> Collapse All
                </button>
                <button type="button" class="btn btn-success create-menu-btn mr-3" data-toggle="modal"
                        data-target="#create-menu-modal" data-parent_id="0">
                    <i class="flaticon2-plus ml-1"></i> Create
                </button>
                @include('layouts._partials.links.preview', ['buttonRoute' => route('Preview::index')])
            </div>
        </div>

        <div class="card-body">
            <div class="row">
                <div class="col-12">
                    <div class="dd" id="nestable3" data-url="{{ route('Menus::updateTree') }}">
                        <ol class="dd-list">
                            @include('menu._partials.element')
                        </ol>
                    </div>
                </div>
            </div>


            @include('menu._partials.modal_create')
            @include('menu._partials.modal_edit')
            @include('menu._partials.modal_delete')
        </div>
    </div>
    <!--end::Card-->
@endsection

@section('page-plugins-styles')
    <link href="{{ asset('theme/css/jquery.nestable.min.css') }}"
          rel="stylesheet" type="text/css"/>
@endsection

@section('page-plugins-scripts')
    <script src="{{ asset('theme/js/jquery.nestable.js') }}"
            type="text/javascript"></script>
@endsection
