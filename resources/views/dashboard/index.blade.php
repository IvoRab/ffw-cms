@extends('layouts.app')

@section('content')
    <!--begin::Card-->
    <div class="card card-custom">
        <div class="card-header flex-wrap pt-4 pb-4">
            <div class="card-title">
                <h3 class="card-label">
                    {{ !empty($pageTitle) ? $pageTitle : '' }}
                </h3>
            </div>
        </div>

        <div class="card-body">
            Dashboard...
        </div>
    </div>
    <!--end::Card-->
@endsection
