@extends('auth.layout')

@section('content')
    <p>Your login session has expired...</p>
    <form class="form" id="" method="POST" data-action="{{ route('Auth::refresh') }}">
        {{ csrf_field() }}
        <button type="submit" id="kt_submit_refresh_token"
                class="btn btn-primary font-weight-bold px-9 py-4 my-3 mx-4">{{ __('common.login_again') }}
        </button>
    </form>
@endsection
