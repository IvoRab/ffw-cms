<button class="btn btn-circle btn-warning btn-xs mr-3 edit-menu-btn" data-toggle="modal"
        data-target="#edit-menu-modal" data-id="{{ $menu['id'] }}"
        data-name="{{ $menu['name'] }}" data-page="{{ $menu['page_id'] }}"
        data-status="{{ $menu['status'] }}" data-is_home_menu="{{ $menu['is_home_menu'] }}">
    <i class="flaticon2-pen ml-1"></i>
</button>
