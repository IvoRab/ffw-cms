<!DOCTYPE html>

<html lang="en">

<!--begin::Head-->
<head>
    <meta charset="utf-8"/>
    <title>{{ env('APP_NAME') }} | {{ $pageTitle ?? '' }}</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta content="http://ffw-cms.test" name="FFW CMS"/>
    <meta name="csrf-token" content="{{ csrf_token() }}"/>

    <!--BEGIN::FONTS-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700"/>
    <!--END::FONTS-->

    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="{{ asset('theme/assets/plugins/global/plugins.bundle.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('theme/assets/plugins/custom/prismjs/prismjs.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('theme/assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css"/>
    <!-- END GLOBAL MANDATORY STYLES -->

    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="{{ asset('theme/assets/css/themes/layout/header/base/light.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('theme/assets/css/themes/layout/header/menu/light.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('theme/assets/css/themes/layout/brand/dark.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('theme/assets/css/themes/layout/aside/dark.css') }}" rel="stylesheet" type="text/css"/>
    <!-- END THEME GLOBAL STYLES -->

    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('img/favicon.ico') }}">

    <!-- BEGIN PAGE PLUGINS STYLES -->
@yield('page-plugins-styles')
<!-- END PAGE PLUGINS STYLES -->

    <!-- BEGIN PAGE STYLES -->
    @yield('page-styles')
    <link href="{{ asset('theme/css/custom.css?v=' . md5(env('APP_VERSION', '1.0.0'))) }}" rel="stylesheet"
          type="text/css"/>
    <!-- END PAGE STYLES -->
</head>
<body id="kt_body" data-page-name="{{ $pageName ?? '' }}"
      class="header-fixed header-mobile-fixed aside-enabled aside-fixed aside-minimize-hoverable page-loading">
{{--    @include('layouts._partials.page_loader')--}}

@include('layouts._partials.header_mobile')
<div class="d-flex flex-column flex-root">
    <div class="d-flex flex-row flex-column-fluid page">
        @include('layouts._partials.sidebar.sidebar')

        <div class="d-flex flex-column flex-row-fluid wrapper" id="kt_wrapper">
            @include('layouts._partials.header')

            <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
                <div class="d-flex flex-column-fluid">

                    <!--begin::Container-->
                    <div class="container-fluid">
                        @yield('content')
                    </div>
                    <!--end::Container-->

                </div>
            </div>

            @include('layouts._partials.footer')
        </div>
    </div>
</div>

@include('layouts._partials._extras.scroll_top')

<!--begin::Global Config(global config for global JS scripts)-->
<script>
    var KTAppSettings = {
        "breakpoints": {
            "sm": 576,
            "md": 768,
            "lg": 992,
            "xl": 1200,
            "xxl": 1400
        },
        "colors": {
            "theme": {
                "base": {
                    "white": "#ffffff",
                    "primary": "#3699FF",
                    "secondary": "#E5EAEE",
                    "success": "#1BC5BD",
                    "info": "#8950FC",
                    "warning": "#FFA800",
                    "danger": "#F64E60",
                    "light": "#E4E6EF",
                    "dark": "#181C32"
                },
                "light": {
                    "white": "#ffffff",
                    "primary": "#E1F0FF",
                    "secondary": "#EBEDF3",
                    "success": "#C9F7F5",
                    "info": "#EEE5FF",
                    "warning": "#FFF4DE",
                    "danger": "#FFE2E5",
                    "light": "#F3F6F9",
                    "dark": "#D6D6E0"
                },
                "inverse": {
                    "white": "#ffffff",
                    "primary": "#ffffff",
                    "secondary": "#3F4254",
                    "success": "#ffffff",
                    "info": "#ffffff",
                    "warning": "#ffffff",
                    "danger": "#ffffff",
                    "light": "#464E5F",
                    "dark": "#ffffff"
                }
            },
            "gray": {
                "gray-100": "#F3F6F9",
                "gray-200": "#EBEDF3",
                "gray-300": "#E4E6EF",
                "gray-400": "#D1D3E0",
                "gray-500": "#B5B5C3",
                "gray-600": "#7E8299",
                "gray-700": "#5E6278",
                "gray-800": "#3F4254",
                "gray-900": "#181C32"
            }
        },
        "font-family": "Poppins"
    };
</script>
<!--end::Global Config-->

<!-- BEGIN GLOBAL MANDATORY SCRIPTS -->
<script src="{{ asset('theme/assets/plugins/global/plugins.bundle.js') }}"></script>
<script src="{{ asset('theme/assets/plugins/custom/prismjs/prismjs.bundle.js') }}"></script>
<script src="{{ asset('theme/assets/js/scripts.bundle.js') }}"></script>
<script src="{{ asset('theme/assets/plugins/custom/datatables/datatables.bundle.js') }}"
        type="text/javascript"></script>

<script type="text/javascript">
    // toastr messages
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };

    //                toastr.success("test alabale");
</script>
@include('layouts._partials.messages')
<!-- END GLOBAL MANDATORY SCRIPTS -->

<!-- BEGIN VENDORS SCRIPTS -->
{{--    <script src="{{ asset('theme/assets/js/pages/features/miscellaneous/toastr.js') }}"></script>--}}
<!-- END VENDORS SCRIPTS -->

<!-- BEGIN PAGE PLUGINS SCRIPTS -->
@yield('page-plugins-scripts')
<!-- BEGIN PAGE PLUGINS SCRIPTS -->

<!-- BEGIN PAGE SCRIPTS -->
@yield('page-scripts')
<script src="{{ asset('theme/js/authentication.js?v=' . md5(env('APP_VERSION', '1.0.0'))) }}"
        type="text/javascript"></script>
<script src="{{ asset('theme/js/custom.js?v=' . md5(env('APP_VERSION', '1.0.0'))) }}" type="text/javascript"></script>
<!-- BEGIN PAGE SCRIPTS -->

</body>
<!--end::Body-->

</html>
