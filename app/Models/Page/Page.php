<?php

namespace App\Models\Page;

use App\Models\Menu\Menu;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Page\Page
 *
 * @property int $id
 * @property int $is_home_page
 * @property string $name
 * @property string $title
 * @property string $body
 * @property string|null $meta_title
 * @property string|null $meta_description
 * @property string $seo_url
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Page\Keyword[] $keywords
 * @property-read int|null $keywords_count
 * @property-read \Illuminate\Database\Eloquent\Collection|Menu[] $menu
 * @property-read int|null $menu_count
 * @property-read \App\Models\Page\PublishQueue|null $publishQueue
 * @method static \Illuminate\Database\Eloquent\Builder|Page newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Page newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Page query()
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereBody($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereIsHomePage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereMetaDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereMetaTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereSeoUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Page extends Model
{
    const MEDIA_PATH = 'page-images';
    protected $table = 'pages';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'is_home_page',
        'name',
        'title',
        'body',
        'meta_title',
        'meta_description',
        'seo_url',
    ];

    public function keywords()
    {
        return $this->belongsToMany(Keyword::class);
    }

    public function publishQueue()
    {
        return $this->hasOne(PublishQueue::class, 'page_id', 'id');
    }

    public function menu()
    {
        return $this->hasMany(Menu::class, 'page_id', 'id');
    }
}
