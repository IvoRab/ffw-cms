<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\TrimStrings as Middleware;
use Illuminate\Support\Facades\Cookie;

class AddToken extends Middleware
{
    public function handle($request, \Closure  $next)
    {
        // If the
        if ($request->cookie(env('JWT_COOKIE_NAME'))) {
            $request->headers->set('Authorization', "Bearer " . $request->cookie(env('JWT_COOKIE_NAME')));
        }

        return $next($request);
    }
}
